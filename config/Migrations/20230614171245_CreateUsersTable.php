<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('users');
        $table->addColumn('username','string')
            ->addColumn('password','string')
            ->addColumn('is_superuser','boolean',['default'=>false])
            ->addColumn('role','string')
            ->addTimestamps('created', 'modified')
            ->addColumn('deleted','datetime',[
                'default' => null,
                'null'=>true
            ]);
        $table->create();
    }
}
