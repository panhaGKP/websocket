<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class CreateScorePoles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('score_poles');
        $table->addColumn('matche_id','integer',[
           'default'=>null,
           'null'=>false,
           'limit'=>11
        ]);
        $table->addColumn('room_code','integer',[
            'default'=>null,
            'null'=>false,
            'limit'=>11
        ]);
        $table->addColumn('pole_number','integer',[
            'default'=>null,
            'null'=>false,
            'limit'=>11
        ]);
        $table->addColumn('red_score','integer',[
            'default'=>null,
            'null'=>false,
            'limit'=>11
        ]);
        $table->addColumn('pole_to_red','boolean',[
            'default'=>false,
            'null'=>false,
        ]);
        $table->addColumn('blue_score','integer',[
            'default'=>null,
            'null'=>false,
            'limit'=>11
        ]);
        $table->addColumn('pole_to_blue','boolean',[
            'default'=>false,
            'null'=>false,
        ]);
        $table->addColumn('created','datetime',[
            'default'=>'CURRENT_TIMESTAMP',
            'null'=>false,
            'limit'=>null
        ]);
        $table->addColumn('modified','datetime',[
            'default'=>null,
            'null'=>true,
            'limit'=>null
        ]);


        $table->create();
    }
}
