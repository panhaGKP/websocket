<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddTimeStampClickToScorePolesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change(): void
    {
        $table = $this->table('score_poles');
        $table->addColumn('timestamp_click_red','biginteger',[
            'after' => 'pole_to_red',
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('timestamp_click_blue','biginteger',[
            'after' => 'pole_to_blue',
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
