<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class AddColumnToMatches extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('matches');
        $table->addColumn('red_first_own_pole_number', 'integer',[
           'default'=>null,
           'null'=>true,
           'limit'=>11
        ]);
        $table->addColumn('red_first_own_pole_at','string',[
           'default'=>null,
           'null'=>true,
           'limit'=>100
        ]);
        $table->addColumn('blue_first_own_pole_number', 'integer',[
           'default'=>null,
           'null'=>true,
           'limit'=>11
        ]);

        $table->addColumn('blue_first_own_pole_at','string',[
            'default'=>null,
            'null'=>true,
            'limit'=>100
        ]);
        $table->update();
    }
}
