<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class RenameColumnToMatches extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('matches');
        $table->renameColumn('bleu_team_score','blue_team_score');
        $table->renameColumn('bleu_team_id','blue_team_id');
        $table->renameColumn('bleu_team_name','blue_team_name');
        $table->update();
    }
}
