<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'id' => '5',
                'username' => 'Panha',
                'password' => '$2y$10$ynFLIjFBwBhRXQhwSwKkNunCDvOqAjSxfKC.Z1RpkRoSFUOhczhUW',
                'is_superuser' => '1',
                'role' => 'admin',
                'created' => '2023-06-16 22:41:09',
                'modified' => '2023-06-16 22:41:09',
                'deleted' => NULL,
            ],
            //pass: <name123>
            [
                'id' => '6',
                'username' => 'vuthy panha',
                'password' => '$2y$10$moP3U4ckixKEcacZ4SExfOGzNW//uIBC9KGj/SSTN89nRHQ5Sscee',
                'is_superuser' => '1',
                'role' => 'admin',
                'created' => '2023-06-17 15:39:07',
                'modified' => '2023-06-17 15:39:07',
                'deleted' => NULL,
            ],
            [
                'id' => '7',
                'username' => 'leang menghang',
                'password' => '$2y$10$HK2gHIU.PSj8SHrDhjiUXuQqjoq86PGa7uRC7SRdZNDSQGOt.Zj0u',
                'is_superuser' => '1',
                'role' => 'admin',
                'created' => '2023-06-17 15:40:13',
                'modified' => '2023-06-17 15:40:13',
                'deleted' => NULL,
            ],
            [
                'id' => '8',
                'username' => 'pang chamnan',
                'password' => '$2y$10$d1/s5ecUEAF1qeMM4/d1De3AyGQJj60/mnZC.ADETwUgaHX2r7mg2',
                'is_superuser' => '1',
                'role' => 'admin',
                'created' => '2023-06-17 15:40:37',
                'modified' => '2023-06-17 15:40:37',
                'deleted' => NULL,
            ],
            [
                'id' => '9',
                'username' => 'mean thydapich',
                'password' => '$2y$10$qz1bASYXMuglBNeAi/z0l.1OWo2sx2y6ve/dJF5cUr1LefitOVxUS',
                'is_superuser' => '1',
                'role' => 'admin',
                'created' => '2023-06-17 15:41:00',
                'modified' => '2023-06-17 15:41:00',
                'deleted' => NULL,
            ],
            [
                'id' => '10',
                'username' => 'yoeurng phearum',
                'password' => '$2y$10$CHr/VcNHjqQPCJZZ3sJ5xek.YskTlt26jIBH3Pm/bXBp507z4Ux.C',
                'is_superuser' => '1',
                'role' => 'admin',
                'created' => '2023-06-17 15:41:37',
                'modified' => '2023-06-17 15:41:37',
                'deleted' => NULL,
            ],
            [
                'id' => '11',
                'username' => 'sim vichetra',
                'password' => '$2y$10$YbzPmN02hVEkhYJF1I/HoeuJfqENVH3AGvh0TsB/v6/cnBWrcalKC',
                'is_superuser' => '1',
                'role' => 'admin',
                'created' => '2023-06-17 15:42:08',
                'modified' => '2023-06-17 15:42:08',
                'deleted' => NULL,
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
