<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Teams seed.
 */
class TeamsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'id' => '15',
                'name' => 'Cambodia',
                'image_path' => 'uploads/team_images/64b5586b2a159.png',
                'created' => '2023-07-17 22:04:11',
                'modified' => '2023-07-17 22:04:11',
                'deleted' => NULL,
            ],
            [
                'id' => '16',
                'name' => 'China',
                'image_path' => 'uploads/team_images/64b5588ead83e.png',
                'created' => '2023-07-17 22:04:46',
                'modified' => '2023-07-17 22:58:56',
                'deleted' => NULL,
            ],
            [
                'id' => '17',
                'name' => 'Egypt',
                'image_path' => 'uploads/team_images/64b565870990b.png',
                'created' => '2023-07-17 23:00:07',
                'modified' => '2023-07-17 23:00:07',
                'deleted' => NULL,
            ],
            [
                'id' => '18',
                'name' => 'Hong Kong',
                'image_path' => 'uploads/team_images/64b56596575e3.png',
                'created' => '2023-07-17 23:00:22',
                'modified' => '2023-07-17 23:00:22',
                'deleted' => NULL,
            ],
            [
                'id' => '19',
                'name' => 'Indonesia',
                'image_path' => 'uploads/team_images/64b565ad6e7ac.png',
                'created' => '2023-07-17 23:00:45',
                'modified' => '2023-07-17 23:00:45',
                'deleted' => NULL,
            ],
            [
                'id' => '20',
                'name' => 'India',
                'image_path' => 'uploads/team_images/64b565ba247bd.png',
                'created' => '2023-07-17 23:00:58',
                'modified' => '2023-07-17 23:00:58',
                'deleted' => NULL,
            ],
            [
                'id' => '21',
                'name' => 'Japan',
                'image_path' => 'uploads/team_images/64b565c40cc5a.png',
                'created' => '2023-07-17 23:01:08',
                'modified' => '2023-07-17 23:01:08',
                'deleted' => NULL,
            ],
            [
                'id' => '22',
                'name' => 'Sri Lanka',
                'image_path' => 'uploads/team_images/64b565ffaf0d0.png',
                'created' => '2023-07-17 23:02:07',
                'modified' => '2023-07-17 23:02:07',
                'deleted' => NULL,
            ],
            [
                'id' => '23',
                'name' => 'Mongolia',
                'image_path' => 'uploads/team_images/64b5666373eee.png',
                'created' => '2023-07-17 23:03:47',
                'modified' => '2023-07-17 23:03:47',
                'deleted' => NULL,
            ],
            [
                'id' => '24',
                'name' => 'Malaysia',
                'image_path' => 'uploads/team_images/64b56699e3437.png',
                'created' => '2023-07-17 23:04:41',
                'modified' => '2023-07-17 23:04:41',
                'deleted' => NULL,
            ],
            [
                'id' => '25',
                'name' => 'Nepal',
                'image_path' => 'uploads/team_images/64b566ac4efdc.png',
                'created' => '2023-07-17 23:05:00',
                'modified' => '2023-07-17 23:05:00',
                'deleted' => NULL,
            ],
            [
                'id' => '26',
                'name' => 'Thailand',
                'image_path' => 'uploads/team_images/64b566c599ba1.png',
                'created' => '2023-07-17 23:05:25',
                'modified' => '2023-07-17 23:05:25',
                'deleted' => NULL,
            ],
            [
                'id' => '27',
                'name' => 'Vietnam',
                'image_path' => 'uploads/team_images/64b5682c1ee0c.png',
                'created' => '2023-07-17 23:11:24',
                'modified' => '2023-07-17 23:11:24',
                'deleted' => NULL,
            ],
        ];

        $table = $this->table('teams');
        $table->insert($data)->save();
    }
}
