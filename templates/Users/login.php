<!-- in /templates/Users/login.php -->
<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\User> $users
 */

$this->setLayout('login')
?>
<!--<div class="users form container">-->
<!--    <h3>Login</h3>-->
<!--    --><?php //= $this->Form->create() ?>
<!--    <fieldset>-->
<!--        <legend>--><?php //= __('Please enter your username and password') ?><!--</legend>-->
<!--        --><?php //= $this->Form->control('username', ['required' => true]) ?>
<!--        --><?php //= $this->Form->control('password', ['required' => true]) ?>
<!--    </fieldset>-->
<!--    --><?php //= $this->Form->submit(__('Login')); ?>
<!--    --><?php //= $this->Form->end() ?>
<!---->
<!--    --><?php //= $this->Html->link("Add User", ['action' => 'add']) ?>
<!--</div>-->
<!-- login.ctp -->
<!-- login.ctp -->
<div class="container">
    <div class="row justify-content-center align-items-center vh-100">
        <div class="col-lg-4">
            <div class="card shadow">
                <div class="card-body">
                    <h2 class="card-title text-center mb-4">Login</h2>
                    <?= $this->Flash->render() ?>
                    <?= $this->Form->create() ?>
                    <div class="mb-3">
                        <?= $this->Form->control('username', [
                            'class' => 'form-control',
                            'required' => true,
                            'label' => false,
                            'placeholder' => 'Username'
                        ]) ?>
                    </div>
                    <div class="mb-3">
                        <?= $this->Form->control('password', [
                            'class' => 'form-control',
                            'required' => true,
                            'label' => false,
                            'placeholder' => 'Password'
                        ]) ?>
                    </div>
                    <div class="d-grid">
                        <?= $this->Form->button('Login', [
                            'class' => 'btn btn-primary btn-block',
                            'type' => 'submit'
                        ]) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->link("Add User", ['action' => 'add']) ?>

