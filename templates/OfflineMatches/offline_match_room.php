<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Matche> $matches
 */
/**
 * @var \App\Model\Entity\Matche $matche
 * @var int $numberOfRedRingPoles
 * @var int $numberOfBlueRingPoles
 * @var int $redScore
 * @var int $blueScore
 * @var int $timerStarted
 */
/**
 * @var integer $roomCode
 * @var array $poleLockCooks
 * @var \App\Model\Entity\Matche $match
 */
$this->setLayout('offline')
?>
<div class="vh-100 border-danger border">
    <!--layout for score operation-->
    <div class="vh-50">
        <div class="mt-0 h-100">
            <div class="d-flex justify-content-center p-3 rounded-3 position-relative">
                <div class="position-absolute  start-0 w-50 text-end"><span class="me-2 fw-bold red p-1 rounded-3"><?= $match->red_team_name ?></span></div>
                <div class="position-absolute  end-0 w-50 text-start"><span class="ms-2 fw-bold blue p-1 rounded-3"><?= $match->blue_team_name ?></span></div>
                <div class="score_operation_match_area vh-45 d-flex position-absolute w-75 top-0">
                    <div class="red_area w-50"></div>
                    <div class="blue_area w-50"></div>
                </div>
                <!--    poles: 1, 2, 3-->
                <div class=" d-flex flex-column justify-content-between align-items-stretch vertical_wrapper mx-5">
                    <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="3">
                        <span class="pole_note">3</span>
                        <div data-lock="<?= $poleLockCooks['pole3']['red'] ?>" data-color="red" data-score="10" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">10</span></div>
                        <div data-lock="<?= $poleLockCooks['pole3']['blue'] ?>" data-color="blue" data-score="25" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">25</span></div>
                    </div>
                    <div class="h-25 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="2">
                        <span class="pole_note">2</span>

                        <div data-lock="<?= $poleLockCooks['pole2']['red'] ?>" data-color="red" data-score="10" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">10</span></div>
                        <div data-lock="<?= $poleLockCooks['pole2']['blue'] ?>" data-color="blue" data-score="25" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">25</span></div>
                    </div>

                    <div class="h-25 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="1">
                        <span class="pole_note">1</span>

                        <div data-lock="<?= $poleLockCooks['pole1']['red'] ?>" data-score="10" data-color="red"  class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">10</span></div>
                        <div data-lock="<?= $poleLockCooks['pole1']['blue'] ?>" data-color="blue" data-score="25" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">25</span></div>
                    </div>


                </div>
                <!--    poles 4, 5-->
                <div class=" justify-content-around d-flex flex-column align-items-stretch mx-4">

                    <div class="h-25 flex-grow-0 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="5">
                        <span class="pole_note">5</span>
                        <div data-lock="<?= $poleLockCooks['pole5']['red'] ?>" data-score="30" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                        <div data-lock="<?= $poleLockCooks['pole5']['blue'] ?>" data-score="30" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
                    </div>
                    <div class="h-25 flex-grow-0 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="4">
                        <span class="pole_note">4</span>
                        <div data-lock="<?= $poleLockCooks['pole4']['red'] ?>" data-score="30"  data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                        <div data-lock="<?= $poleLockCooks['pole4']['blue'] ?>" data-score="30"  data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
                    </div>
                </div>
                <!--    poles 6-->
                <div class="justify-content-center d-flex flex-column align-items-stretch mx-4">
                    <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="6">
                        <span class="pole_note">6</span>

                        <div data-lock="<?= $poleLockCooks['pole6']['red'] ?>" data-score="70"  data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">70</span></div>
                        <div data-lock="<?= $poleLockCooks['pole6']['blue'] ?>" data-score="70"  data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">70</span></div>
                    </div>
                </div>

                <!--    poles 7, 8-->
                <div class="justify-content-around d-flex flex-column align-items-stretch mx-4">
                    <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="8">
                        <span class="pole_note">8</span>
                        <div data-lock="<?= $poleLockCooks['pole8']['red'] ?>" data-score="30"  data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                        <div data-lock="<?= $poleLockCooks['pole8']['blue'] ?>" data-score="30"  data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
                    </div>
                    <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="7">
                        <span class="pole_note">7</span>
                        <div data-lock="<?= $poleLockCooks['pole7']['red'] ?>" data-score="30"  data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                        <div data-lock="<?= $poleLockCooks['pole7']['blue'] ?>" data-score="30"  data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
                    </div>

                </div>
                <!--    poles 9, 10, 11 -->
                <div class="justify-content-between d-flex flex-column align-items-stretch mx-4">
                    <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="11">
                        <span class="pole_note">11</span>
                        <div data-lock="<?= $poleLockCooks['pole11']['red'] ?>" data-score="25"   data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class=""></span>25</div>
                        <div data-lock="<?= $poleLockCooks['pole11']['blue'] ?>" data-score="10"   data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">10</span></div>
                    </div>
                    <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="10">
                        <span class="pole_note">10</span>
                        <div data-lock="<?= $poleLockCooks['pole10']['red'] ?>" data-score="25"  data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class=""></span>25</div>
                        <div data-lock="<?= $poleLockCooks['pole10']['blue'] ?>" data-score="10"  data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">10</span></div>
                    </div>
                    <div class="h-25 d-flex align-items-center  border pole_wrapper bg-white p-3 rounded-3" data-pole-number="9">
                        <span class="pole_note">9</span>
                        <div data-lock="<?= $poleLockCooks['pole9']['red'] ?>" data-score="25"  data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class=""></span>25</div>
                        <div data-lock="<?= $poleLockCooks['pole9']['blue'] ?>" data-score="10"  data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">10</span></div>
                    </div>
                </div>

            </div>
            <div class="mt-3">
                <div class="d-flex justify-content-around align-items-center mb-3">
                    <button class="btn btn-primary shadow-sm" id="start-timer"><i class="bi bi-alarm me-1"></i>Timer Start</button>
                    <div id="room_code" data-room-code="<?= $roomCode ?>" class="text-center">Room code: <?= $roomCode ?></div>
                    <div>
                        <button class="btn btn-purple shadow-sm d-none" id="test-connection"><i class="bi bi-broadcast me-1"></i>Test Connection</button>
                        <button type="button" class="btn btn-warning shadow-sm" data-bs-toggle="modal" data-bs-target="#resetScoreModal">
                            <i class="bi bi-arrow-repeat me-1"></i>Reset Score
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="alert alert-success alert-dismissible fade position-absolute bottom-0 end-0 me-2" role="alert" id="success-flash">
            <strong>Connection success</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <div class="alert alert-warning alert-dismissible fade position-absolute bottom-0 end-0 me-2" role="alert" id="fail-flash">
            <strong>Connection fail</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <div class="d-none" id="connection" data-connection="0"></div>
        <!-- Modal -->
        <div class="modal fade" id="resetScoreModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to reset the score?</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-warning text-center" role="alert">
                            Make sure the member know about this. You reset the score in room #<?= $roomCode ?>.
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger text-center" id="reset-score"><span class="confirm-letter">Confirm</span>
                            <span class="spinner-border spinner-border-sm text-light d-none" id="loading-confirm" role="status">
                    </span>
                        </button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--layout for scoreboard-->
    <div class="vh-50 ">
        <?php echo $this->Form->control('timer', [
            'type' => 'hidden',
            'value' => $matche->timer]) ?>
        <?php echo $this->Form->control('timerStart', [
            'type' => 'hidden',
            'value' => $timerStarted]) ?>
        <div class="">
            <h2 class="fs-1 text-center d-none">Room Match</h2>
            <audio id="prepare_sound">
                <source src="/audio/audio_prepare.MP3" type="audio/mpeg">
            </audio>
            <audio id="start_sound">
                <source src="/audio/count1min_audio.MP3" type="audio/mpeg">
            </audio>
            <audio id="finish_sound">
                <source src="/audio/finish_sound.MP3" type="audio/mpeg">
            </audio>
            <audio id="almost_finish_sound">
                <source src="/audio/almost_finish_audio.MP3" type="audio/mpeg">
            </audio>

            <div class="d-none" id="red_first_own" data-red-first-own="0"></div>
            <div class="d-none" id="blue_first_own" data-blue-first-own="0"></div>

            <div id="timer_pause" data-timer-pause="0" class="d-none"></div>
            <h5 class="text-center text-black-50 d-none" id="room_code" data-room-code="<?= $matche->room_code ?>">Room code: <?= $matche->room_code ?></h5>
            <?= $this->Html->link('<i class="bi bi-arrows-fullscreen"></i>', '#', ['id' => 'fullscreen-btn', 'escape' => false,'class' => 'rounded-2 mb-2 expand_icon shadow p-2 fs-4 d-none']) ?>

            <!--new screen-->
            <div class="mx-auto border"  id="score_new">
                <div id="score_board" class="d-none rounded-3 position-relative bg-white d-flex flex-column">
                    <?= $this->Html->image('top_background_transparent.svg', ['class' => 'position-absolute top-0 start-0 w-100 z-0']) ?>
                    <!--Top title of the score board-->
                    <div class="w-100 mt-1 title_of_scor_board">
                        <div class="d-flex mx-auto justify-content-center align-items-center ">
                            <?= $this->Html->image('2023_logo_robot_PNG.png', ['class' => 'z-1 robocon23_logo']) ?>
                            <p class="title_robocon_in_khmer mt-3">ការប្រកួតប្រជែងរ៉ូបូតថ្នាក់ជាតិលើកទី១០ ឆ្នាំ២០២៣</p>
                        </div>
                    </div>
                    <div class="d-flex flex-column flex-grow-1 p-2 " style="z-index: 100;">
                        <div class="score_board_display_section flex-grow-1 d-flex justify-content-center align-items-center mb-1">
                            <!--score for red team-->
                            <div class="h-100 bg-red w-50 angle_round me-1 border">
                                <div class="text-center letter_team_name mt-2"><?= $matche->red_team_name ?></div>
                                <div class="bg_of_score_number w-70 mx-auto h-60 d-flex align-items-center justify-content-center mb-2">
                                    <div id="red_score" class="text-center letter_score"><?= $redScore ?></div>
                                </div>
                            </div>
                            <!--score for blue team-->
                            <div class="h-100 border border-primary bg-bleu w-50 angle_round ms-1">
                                <div class="text-center letter_team_name"><?= $matche->blue_team_name ?></div>
                                <!--                    <div id="blue_score" class="text-center letter_score">--><?php //= $blueScore ?><!--</div>-->
                                <div class="bg_of_score_number w-70 mx-auto h-60 d-flex align-items-center justify-content-center mb-2">
                                    <div id="blue_score" class="text-center letter_score"><?= $blueScore ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="below_score_board_section  d-flex justify-content-between align-items-center mt-1">
                            <!--number of korng in red-->
                            <div class="w-25  h-100 me-1 bg-red angle_round d-flex  justify-content-center align-items-center">
                                <div id="red_pole" class=" unknown letter_pole text-center d-flex align-items-center justify-content-center">
                                    <?= $numberOfRedRingPoles ?>
                                </div>
                            </div>
                            <!--Timer-->
                            <div class="bg-black w-50 h-100 angle_round mx-1 d-flex justify-content-center align-items-center">
                                <div id="timer_display" class="letter_timer text-center text-white d-flex align-items-center justify-content-center">1</div>
                            </div>
                            <!--number of korng in blue-->
                            <div class="w-25 bg-bleu h-100 angle_round ms-1 d-flex justify-content-center align-items-center">
                                <div id="blue_pole" class="unknown letter_pole text-center justify-content-center align-items-center">
                                    <?= $numberOfBlueRingPoles ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="chey_yo_modal_bg d-none" id="chey_yo_modal">
                        <div class="w-100 h-100">
                            <div class="chey_yo_content rounded-3 w-50 h-25 shadow position-relative text-center d-flex justify-content-center align-items-center">
                                <span class="winner_animation_text fw-bold fst-italic">CHEY - YO</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="countdown_prepare_div" class="border-secondary border bg-white position-relative">
                    <span id="countdown_prepare" class="countdown-10 letter">10</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script(['offline.js?t=1']);
?>


