<?php

/**
 * @var \App\View\AppView $this
 * @var integer $roomCode
 * @var array $poleLockCooks
 * @var \App\Model\Entity\Matche $match
 */
?>

<div class="">
    <div class="d-flex justify-content-around mt-4 shadow-sm border p-3 rounded-3 position-relative">
        <div class="position-absolute  start-0 w-50 text-end"><span class="me-2 fw-bold red p-1 rounded-3">
                <?= $match->red_team_name ?>
            </span></div>
        <div class="position-absolute  end-0 w-50 text-start"><span class="ms-2 fw-bold blue p-1 rounded-3">
                <?= $match->blue_team_name ?>
            </span></div>
        <div class="match_area d-flex position-absolute w-100 h-100 top-0">
            <div class="red_area w-50"></div>
            <div class="blue_area w-50"></div>
        </div>
        <!--    poles: 1, 2, 3-->
        <div class="d-flex flex-column justify-content-between align-items-stretch vertical_wrapper">
            <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="3">
                <span class="pole_note">3</span>
                <div data-lock="<?= $poleLockCooks['pole3']['red'] ?>" data-color="red" data-score="10" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">10</span></div>
                <div data-lock="<?= $poleLockCooks['pole3']['blue'] ?>" data-color="blue" data-score="25" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">25</span></div>
            </div>
            <div class="h-25 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="2">
                <span class="pole_note">2</span>

                <div data-lock="<?= $poleLockCooks['pole2']['red'] ?>" data-color="red" data-score="10" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">10</span></div>
                <div data-lock="<?= $poleLockCooks['pole2']['blue'] ?>" data-color="blue" data-score="25" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">25</span></div>
            </div>

            <div class="h-25 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="1">
                <span class="pole_note">1</span>

                <div data-lock="<?= $poleLockCooks['pole1']['red'] ?>" data-score="10" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">10</span></div>
                <div data-lock="<?= $poleLockCooks['pole1']['blue'] ?>" data-color="blue" data-score="25" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">25</span></div>
            </div>


        </div>
        <!--    poles 4, 5-->
        <div class="justify-content-around d-flex flex-column align-items-stretch">

            <div class="h-25 flex-grow-0 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="5">
                <span class="pole_note">5</span>
                <div data-lock="<?= $poleLockCooks['pole5']['red'] ?>" data-score="30" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole5']['blue'] ?>" data-score="30" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
            </div>
            <div class="h-25 flex-grow-0 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="4">
                <span class="pole_note">4</span>
                <div data-lock="<?= $poleLockCooks['pole4']['red'] ?>" data-score="30" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole4']['blue'] ?>" data-score="30" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
            </div>
        </div>
        <!--    poles 6-->
        <div class="justify-content-center d-flex flex-column align-items-stretch">
            <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="6">
                <span class="pole_note">6</span>

                <div data-lock="<?= $poleLockCooks['pole6']['red'] ?>" data-score="70" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">70</span></div>
                <div data-lock="<?= $poleLockCooks['pole6']['blue'] ?>" data-score="70" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">70</span></div>
            </div>
        </div>

        <!--    poles 7, 8-->
        <div class="justify-content-around d-flex flex-column align-items-stretch">
            <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="8">
                <span class="pole_note">8</span>
                <div data-lock="<?= $poleLockCooks['pole8']['red'] ?>" data-score="30" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole8']['blue'] ?>" data-score="30" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
            </div>
            <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="7">
                <span class="pole_note">7</span>
                <div data-lock="<?= $poleLockCooks['pole7']['red'] ?>" data-score="30" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole7']['blue'] ?>" data-score="30" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">30</span></div>
            </div>

        </div>
        <!--    poles 9, 10, 11 -->
        <div class="justify-content-between d-flex flex-column align-items-stretch">
            <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="11">
                <span class="pole_note">11</span>
                <div data-lock="<?= $poleLockCooks['pole11']['red'] ?>" data-score="25" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class=""></span>25</div>
                <div data-lock="<?= $poleLockCooks['pole11']['blue'] ?>" data-score="10" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">10</span></div>
            </div>
            <div class="h-25 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="10">
                <span class="pole_note">10</span>
                <div data-lock="<?= $poleLockCooks['pole10']['red'] ?>" data-score="25" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class=""></span>25</div>
                <div data-lock="<?= $poleLockCooks['pole10']['blue'] ?>" data-score="10" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">10</span></div>
            </div>
            <div class="h-25 d-flex align-items-center  border pole_wrapper bg-white p-3 rounded-3" data-pole-number="9">
                <span class="pole_note">9</span>
                <div data-lock="<?= $poleLockCooks['pole9']['red'] ?>" data-score="25" data-color="red" class="circle outline_red align-items-center d-flex justify-content-center me-2"><span class=""></span>25</div>
                <div data-lock="<?= $poleLockCooks['pole9']['blue'] ?>" data-score="10" data-color="blue" class="circle outline_blue align-items-center d-flex justify-content-center"><span class="">10</span></div>
            </div>
        </div>

    </div>
    <div>
        <div id="room_code" data-room-code="<?= $roomCode ?>" class="text-center">Room code: <?= $roomCode ?></div>
        <div class="d-flex justify-content-around align-items-center mb-3">
            <button class="btn btn-primary shadow-sm" id="start-timer"><i class="bi bi-alarm me-1"></i>Timer
                Start</button>
            <div>
                <button class="btn btn-purple shadow-sm" id="test-connection"><i class="bi bi-broadcast me-1"></i>Test
                    Connection</button>
                <button type="button" class="btn btn-warning shadow-sm" data-bs-toggle="modal" data-bs-target="#resetScoreModal">
                    <i class="bi bi-arrow-repeat me-1"></i>Reset Score
                </button>
            </div>
        </div>
    </div>
</div>
<div class="alert alert-success alert-dismissible fade position-absolute bottom-0 end-0 me-2" role="alert" id="success-flash">
    <strong>Connection success</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<div class="alert alert-warning alert-dismissible fade position-absolute bottom-0 end-0 me-2" role="alert" id="fail-flash">
    <strong>Connection fail</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
<div class="d-none" id="connection" data-connection="0"></div>
<!-- Modal -->
<div class="modal fade" id="resetScoreModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to reset the score?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning text-center" role="alert">
                    Make sure the member know about this. You reset the score in room #
                    <?= $roomCode ?>.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger text-center" id="reset-score"><span class="confirm-letter">Confirm</span>
                    <span class="spinner-border spinner-border-sm text-light d-none" id="loading-confirm" role="status">
                    </span>
                </button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script(['pusher.min.js?t=' . time()]);
?>
<script>
    $(document).ready(function() {
        // $('#resetScoreModal').modal('show');
        let roomCode = $('#room_code').data('room-code');
        let pusher = new Pusher("1eaf2f48999ec6fdee2b", {
            cluster: 'ap1',
            // encrypted: true,
        });

        let connectionRespond = pusher.subscribe('respondConnection');
        connectionRespond.bind('test', function(data) {
            console.log(data)
            $('#connection').data('connection', data.connection)
            // console.log($('#connection').data('connection'))
            if (parseInt(data.connection)) {
                flashSuccess()
            }
        })

        applyBgToLockButton();

        $('#start-timer').on('click', function() {
            console.log('start time started');
            let roomCode = $('#room_code').data('room-code');
            console.log(roomCode)
            $.ajax({
                url: "/matches/startTimer",
                method: "post",
                dataType: "json",
                data: {
                    roomCode: roomCode,
                }
            })
        })

        $('.circle').on('click', function() {
            /*==Change color =============*/
            let parentDiv = $(this).parent();
            console.log('circle click');
            let color1 = $(this).data('color');
            // console.log('this click :'+color1);
            $(this).addClass(color1);
            let $otherCircle = parentDiv.find('.circle').not(this);
            let color2 = $otherCircle.data('color');
            // console.log('other color :'+color2);
            $otherCircle.removeClass(color2);

            //lock value
            let thisLock = $(this).data('lock');
            let otherLock = $otherCircle.data('lock')

            /*====Request update score=======*/
            let roomCode = $('#room_code').data('room-code');
            let poleNumber = parentDiv.data('pole-number');
            let score = $(this).data('score');
            let teamColor = $(this).data('color');
            // console.log(poleNumber === 1);
            if (thisLock) {
                console.log('lock request');
                return;
            }

            if (poleNumber === 1 || poleNumber === 2 || poleNumber === 3) {
                let xhr1 = $.ajax({
                    method: 'post',
                    url: '/matches/scoreForPole123',
                    dataType: 'json',
                    data: {
                        pole_number: poleNumber,
                        team_color: teamColor,
                        score: score,
                        room_code: roomCode,
                    }
                })
                xhr1.done(function(response) {
                    console.log('done update pole ' + response.poleNumber)
                })
            } else if (poleNumber === 6 || poleNumber === 5 || poleNumber === 4 || poleNumber === 7 || poleNumber === 8) {
                let xhr1 = $.ajax({
                    method: 'post',
                    url: '/matches/scoreForPole45678',
                    dataType: 'json',
                    data: {
                        pole_number: poleNumber,
                        team_color: teamColor,
                        score: score,
                        room_code: roomCode,
                    }
                })
                xhr1.done(function(response) {
                    console.log('done update pole ' + response.poleNumber)
                })
            } else {

                let xhr1 = $.ajax({
                    method: 'post',
                    url: '/matches/scoreForPole91011',
                    dataType: 'json',
                    data: {
                        pole_number: poleNumber,
                        team_color: teamColor,
                        score: score,
                        room_code: roomCode,
                    }
                })
                xhr1.done(function(response) {
                    console.log('done update pole ' + response.poleNumber)
                })
            }

            //lock yourself and unlock request for other
            $(this).data('lock', 1);
            $otherCircle.data('lock', 0)

            /*console.log('this lock: '+thisLock)
            console.log('other Lock: '+otherLock)*/
        })
        $('#reset-score').on('click', function() {
            $('.confirm-letter').addClass('d-none');
            $('#loading-confirm').removeClass('d-none')
            $(this).addClass('disabled');
            console.log(roomCode)
            let xhr1 = $.ajax({
                method: 'get',
                url: '/matches/resetScoreMatch/' + roomCode,
                dataType: 'json',
                data: {}
            })
            xhr1.done(function(response) {
                console.log(parseInt(response.roomCode) !== roomCode)
                if (parseInt(response.roomCode) !== roomCode) {
                    return;
                }
                if (parseInt(response.code) === 200) {
                    let $circle = $('.circle');
                    //reset lock
                    $circle.data('lock', 0);
                    //reset bg
                    $circle.removeClass('red blue');
                    $('#resetScoreModal').modal('hide');

                    $('.confirm-letter').removeClass('d-none');
                    $('#loading-confirm').addClass('d-none')
                    $('#reset-score').removeClass('disabled');
                }

            })
        })

        $('#test-connection').on('click', function() {
            let $connectEle = $('#connection');
            //remove class show from alert
            let connection = $connectEle.data('connection');
            let timeInterval = 5;
            // console.log(connection)
            //make ajax request
            let xhr1 = $.ajax({
                method: 'post',
                url: '/matches/broadcastRequestConnection',
                dataType: 'json',
                data: {
                    room_code: roomCode
                }
            })
            let connectionInterval = setInterval(function() {
                timeInterval--;
                console.log(connection);
                if (timeInterval === 0) {
                    clearInterval(connectionInterval)
                    // display the alert of internet connection
                    let newConnectionTest = $connectEle.data('connection');
                    // console.log('connection: '+newConnectionTest)
                    if (!parseInt(newConnectionTest)) {
                        flashFail();
                    }
                    //reset the connection
                    $connectEle.data('connection', 0)

                }
            }, 1000);
        })

        function flashSuccess() {
            $('#success-flash').addClass('show');
        }

        function flashFail() {
            // console.log('connection fail')
            $('#fail-flash').addClass('show');
        }

        function applyBgToLockButton() {
            // console.log('hello')
            let $this = $('.circle');
            $this.each(function() {
                let bgColorLock = $(this).data('color');
                let isLock = $(this).data('lock');
                if (isLock) {
                    $(this).addClass(bgColorLock)
                }
            })
        }
    })
</script>
