<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Matche $matche
 * @var array $teams
 * @var int $randomRoomCode
 */
//debug($teams);
?>
<div class="container mb-2">

    <div class="column-responsive column-80">
        <div class="matches form content">
            <?= $this->Form->create($matche) ?>
            <fieldset>
                <legend class="fs-1"><?= __('Create new Match') ?></legend>
                <label class="fs-2 mt-4">Set The Match</label>
                <div class="d-flex mt-3">
                    <div class="d-flex align-content-center parent_div">
                        <div>
                            <?php echo $this->Form->control('red_team_id', ['label' => false,'class' => '','options' => $teams,[]]); ?>
                        </div>
                        <div class="red_label"></div>

                    </div>
                    <div class="mx-5 text-center justify-content-center align-content-center mt-2">
                        VS
                    </div>

                    <div class="d-flex align-content-center parent_div">
                        <div class="bleu_label"></div>

                        <div>
                            <?php echo $this->Form->control('blue_team_id', ['label' => false,'class' => '','options' => $teams]); ?>
                        </div>
                    </div>
                </div>
                <div class="d-flex">
                    <?php
                    echo $this->Form->control('timer', [
                        'label' => [
                            'text' => 'Set the time (minute)',
                            'class' => 'fs-2 mt-3',
                        ],
                        'class' => ['w-50 text-center shadow-sm'],
                        'default' => 3,
                    ]);
                    ?>
                </div>
                <label class="fs-2 mt-3">Room Code</label>
                <div class="d-flex align-items-center">
                    <?php
                    echo $this->Form->control('room_code', ['label' => false,'value' => $randomRoomCode,'class' => 'text-center shadow-sm']);
                    ?>
                    <div class="ms-2"><a href="#" id="room_code">Copy</a></div>
                </div>
                <label class="fs-2 mt-3">Match Note</label>

                <select name="single_selected_note" class="form-select w-50" aria-label="Default select example">
                    <option value>Choose one</option>
                    <option value="ការប្រកួតសាកល្បងលើកទី១">ការប្រកួតសាកល្បងលើកទី១</option>
                    <option value="ការប្រកួតសាកល្បងលើកទី២">ការប្រកួតសាកល្បងលើកទី២</option>
                    <option value="ការប្រកួតវគ្គជម្រុះក្នុងពូល">ការប្រកួតវគ្គជម្រុះក្នុងពូលជុំទី១</option>
                    <option value="ការប្រកួតវគ្គជម្រុះក្នុងពូល">ការប្រកួតវគ្គជម្រុះក្នុងពូលជុំទី២</option>
                    <option value="ការប្រកួតវគ្គជម្រុះក្នុងពូល">ការប្រកួតវគ្គជម្រុះក្នុងពូលជុំទី៣</option>
                    <option value="ការប្រកួតវគ្គជម្រុះក្នុងពូល">ការប្រកួតវគ្គជម្រុះក្នុងពូលជុំទី៣</option>
                    <option value="ការប្រកួតវគ្គជម្រុះក្នុងពូល">ការប្រកួតវគ្គ៨ក្រុម</option>
                    <option value="ការប្រកួតវគ្គពាក់កណ្តាលផ្តាច់ព្រាត់">ការប្រកួតវគ្គពាក់កណ្តាលផ្តាច់ព្រាត់</option>
                    <option value="ការប្រកួតវគ្គផ្តាច់ព្រាត់">ការប្រកួតវគ្គផ្តាច់ព្រាត់</option>
                    <option value="ការប្រកួតយកលេខ៣">ការប្រកួតយកលេខ៣</option>
                </select>

                <div class="mt-1">
                    <?php
                        echo $this->Form->control('note', ['label' => false,'class' => 'shadow-sm w-25','placeholder'=>'If none of the options above is match, please leave the note here!']);
                    ?>
                </div>
            </fieldset>
            <?= $this->Form->button(__('Create'), ['class' => 'btn btn-primary mt-3']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

</div>
<script>
    $(document).ready(function (){
        $('#room_code').on('click',function (){
            let input = $('#room-code').val()
            navigator.clipboard.writeText(input)
                .then(()=>{
                    console.log('Text copy to clipboard')
                })
                .catch((error)=>{
                    console.error('Fail to copy, error: ', error)
                });
        })
    })
</script>
