<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Matche> $matches
 * @var \App\Model\Entity\Matche $matche
 */
?>


<div  class="row  vh-100">
    <div class="col d-flex justify-content-center align-items-center">
        <div class="w-50">
            <?= $this->Form->create()?>
            <?= $this->Form->control('room_code',['label'=>[
                'text'=>'Enter the Room Code',
                'class'=>'fw-bold'
            ],
                'placeholder'=>'1234',
                'class'=>'form-control form-control-lg shadow-sm w-100 text-center','type'=>'number']) ?>
            <div class="d-flex justify-content-center">
                <?= $this->Form->button(__('Submit'),['class'=>'btn btn-primary mt-2 rounded']) ?>

            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
