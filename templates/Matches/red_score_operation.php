<?php

/**
 * @var \App\View\AppView $this
 * @var int $roomCode
 * @var array $poleLockCooks
 * @var \App\Model\Entity\Matche $match
 * @var \App\Model\Entity\Team $redTeams
 * @var \App\Model\Entity\Team $blueTeams
 */
?>

<div class="container">
    <div class="d-flex justify-content-around mt-4 shadow-sm border p-3 rounded-3 position-relative">
        <div class="position-absolute  start-0 w-50 text-end">
            <span class="me-2 fw-bold red p-2 rounded-3 justify-content-center align-items-center">
                <span>
                    <?= $match->red_team_name ?>
                </span>
                <span>
                    <?= $this->Html->image($redTeams->image_path, ['class' => 'small-image']) ?>
                </span>
            </span>
        </div>
        <div class="position-absolute  end-0 w-50 text-start">
            <span class="ms-2 fw-bold blue p-2 rounded-3 justify-content-center align-items-center">
                <span>
                    <?= $this->Html->image($blueTeams->image_path, ['class' => 'small-image']) ?>
                </span>
                <span>
                    <?= $match->blue_team_name ?>
                </span>
            </span>
        </div>
        <div class="match_area d-flex position-absolute w-100 h-100 top-0">
            <div class="red_area w-50"></div>
            <div class="blue_area w-50"></div>
        </div>
        <!--    poles: 1, 2, 3-->
        <div class="d-flex flex-column justify-content-between align-items-stretch vertical_wrapper">
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center border pole_wrapper bg-white p-4 rounded-3" data-pole-number="3">
                <span class="pole_note">3</span>
                <div data-lock="<?= $poleLockCooks['pole3']['red'] ?>" data-color="red" data-score="10" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">10</span></div>
                <div data-lock="<?= $poleLockCooks['pole3']['blue'] ?>" data-color="blue" data-score="25" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">25</span>
                </div>
            </div>
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="2">
                <span class="pole_note">2</span>
                <div data-lock="<?= $poleLockCooks['pole2']['red'] ?>" data-color="red" data-score="10" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">10</span></div>
                <div data-lock="<?= $poleLockCooks['pole2']['blue'] ?>" data-color="blue" data-score="25" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">25</span>
                </div>
            </div>

            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="1">
                <span class="pole_note">1</span>
                <div data-lock="<?= $poleLockCooks['pole1']['red'] ?>" data-score="10" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">10</span></div>
                <div data-lock="<?= $poleLockCooks['pole1']['blue'] ?>" data-color="blue" data-score="25" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">25</span>
                </div>
            </div>
        </div>
        <!--    poles 4, 5-->
        <div class="justify-content-around d-flex flex-column align-items-stretch">

            <div class="position-relative d-flex justify-content-center align-items-center h-30 flex-grow-0 d-flex align-items-center mt-2 border pole_wrapper bg-white p-4 rounded-3" data-pole-number="5">
                <span class="pole_note">5</span>
                <div data-lock="<?= $poleLockCooks['pole5']['red'] ?>" data-score="30" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole5']['blue'] ?>" data-score="30" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">30</span>
                </div>
            </div>
            <div class="position-relative d-flex justify-content-center align-items-center h-30 flex-grow-0 d-flex align-items-center mt-2 border pole_wrapper bg-white p-3 rounded-3" data-pole-number="4">
                <span class="pole_note">4</span>
                <div data-lock="<?= $poleLockCooks['pole4']['red'] ?>" data-score="30" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole4']['blue'] ?>" data-score="30" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">30</span>
                </div>
            </div>
        </div>
        <!--    poles 6-->
        <div class="justify-content-center d-flex flex-column align-items-stretch">
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center border pole_wrapper bg-white p-4 rounded-3" data-pole-number="6">
                <span class="pole_note">6</span>
                <div data-lock="<?= $poleLockCooks['pole6']['red'] ?>" data-score="70" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">70</span></div>
                <div data-lock="<?= $poleLockCooks['pole6']['blue'] ?>" data-score="70" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">70</span>
                </div>
            </div>
        </div>

        <!--    poles 7, 8-->
        <div class="justify-content-around d-flex flex-column align-items-stretch">
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center border pole_wrapper bg-white p-4 rounded-3" data-pole-number="8">
                <span class="pole_note">8</span>
                <div data-lock="<?= $poleLockCooks['pole8']['red'] ?>" data-score="30" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole8']['blue'] ?>" data-score="30" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">30</span>
                </div>
            </div>
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center border pole_wrapper bg-white p-3 rounded-3" data-pole-number="7">
                <span class="pole_note">7</span>
                <div data-lock="<?= $poleLockCooks['pole7']['red'] ?>" data-score="30" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class="">30</span></div>
                <div data-lock="<?= $poleLockCooks['pole7']['blue'] ?>" data-score="30" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">30</span>
                </div>
            </div>

        </div>
        <!--    poles 9, 10, 11 -->
        <div class="justify-content-between d-flex flex-column align-items-stretch">
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center border pole_wrapper bg-white p-4 rounded-3" data-pole-number="11">
                <span class="pole_note">11</span>
                <div data-lock="<?= $poleLockCooks['pole11']['red'] ?>" data-score="25" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class=""></span>25</div>
                <div data-lock="<?= $poleLockCooks['pole11']['blue'] ?>" data-score="10" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">10</span>
                </div>
            </div>
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center border pole_wrapper bg-white p-4 rounded-3" data-pole-number="10">
                <span class="pole_note">10</span>
                <div data-lock="<?= $poleLockCooks['pole10']['red'] ?>" data-score="25" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class=""></span>25</div>
                <div data-lock="<?= $poleLockCooks['pole10']['blue'] ?>" data-score="10" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">10</span>
                </div>
            </div>
            <div class="position-relative d-flex justify-content-center align-items-center h-30 d-flex align-items-center  border pole_wrapper bg-white p-4 rounded-3" data-pole-number="9">
                <span class="pole_note">9</span>
                <div data-lock="<?= $poleLockCooks['pole9']['red'] ?>" data-score="25" data-color="red" class="red_circle outline_red align-items-center d-flex justify-content-center"><span class=""></span>25</div>
                <div data-lock="<?= $poleLockCooks['pole9']['blue'] ?>" data-score="10" data-color="blue" class="position-absolute top-1 start-1 blue_small_circle outline_blue align-items-center d-flex justify-content-center">
                    <span class="">10</span>
                </div>
            </div>
        </div>

    </div>
    <div class="position-relative">
        <div id="room_code" data-room-code="<?= $roomCode ?>" class="text-center">Room code: <?= $roomCode ?></div>
        <div class="d-flex justify-content-around align-items-center mb-3">
            <button class="btn btn-primary shadow-sm" id="start-timer"><i class="bi bi-alarm me-1"></i>Timer
                Start</button>
            <div>
                <button class="btn btn-purple shadow-sm" id="test-connection"><i class="bi bi-broadcast me-1"></i>Test
                    Connection</button>
                <button type="button" class="btn btn-warning shadow-sm" data-bs-toggle="modal" data-bs-target="#resetScoreModal">
                    <i class="bi bi-arrow-repeat me-1"></i>Reset Score
                </button>
            </div>
        </div>
        <div class="alert alert-success alert-dismissible fade position-absolute bottom-0 end-0 me-2" role="alert" id="success-flash">
            <strong>Connection success</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <div class="alert alert-warning alert-dismissible fade position-absolute bottom-0 end-0 me-2" role="alert" id="fail-flash">
            <strong>Connection fail</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
</div>

<div class="d-none" id="connection" data-connection="0"></div>
<div class="d-none" id="last-lock-issued" data-last-lock-issued="0"></div>
<!-- Modal -->
<div class="modal fade" id="resetScoreModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to reset the score?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning text-center" role="alert">
                    Make sure the member know about this. You reset the score in room #
                    <?= $roomCode ?>.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger text-center" id="reset-score"><span class="confirm-letter">Confirm</span>
                    <span class="spinner-border spinner-border-sm text-light d-none" id="loading-confirm" role="status">
                    </span>
                </button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!--Modal of Chey-yo alert to stop from operation -->
<div class="modal fade" id="alertStopOperation" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Stop Operation!!!</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <span id="who_chey_yo"></span> got CHEY - YO. Please stop your operation!!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    console.log(<?= json_encode($poleLockCooks) ?>)
</script>
<?php
echo $this->Html->script(['pusher.min.js?t=' . time(), 'red_score_operation.js?t=' . time()]);
?>
<!--<script>-->
<!--    $(document).ready(function() {-->
<!--        -->
<!--    })-->
<!--</script>-->
