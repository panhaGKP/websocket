<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Matche $match
 * @var \App\Model\Entity\Team $redTeams
 * @var \App\Model\Entity\Team $blueTeams
 * @var integer $numberOfBluePole
 * @var integer $numberOfRedPole
 * @var integer $roomCode
 * @var integer $timerStarted
 * @var array $poleTypeCooks
 * @var boolean $isCheyyoEnable
 */
$this->setLayout('streaming')
?>

<?= $this->Html->image('Roobocon-Live-2.png', ['class' => 'position-absolute']) ?>

<?php echo $this->Form->control('timer', [
    'type' => 'hidden',
    'value' => $match->timer]) ?>
<?php echo $this->Form->control('timerStart', [
    'type' => 'hidden',
    'value' => $timerStarted]) ?>
<!--other important value-->
<div class="d-none" id="roomCode" data-room-code="<?= $roomCode ?>"></div>
<div id="timer_pause" data-timer-pause="0" class="d-none"></div>
<div class="d-none" id="is_cheyyo_enable" data-cheyyo-enable="<?= $isCheyyoEnable ?>"></div>


<div class="position-relative vh-100 w-100  border-danger">
    <div class="flag_wrapper w-100 position-absolute">
        <div class="flag_container d-flex justify-content-between">
            <div class="margin-start-red-flag">
                <?= $this->Html->image($redTeams->image_path, ['class' => 'team_flag']) ?>
            </div>
            <div class="margin-end-blue-flag">
                <?= $this->Html->image($blueTeams->image_path, ['class' => 'team_flag']) ?>
            </div>
        </div>
    </div>
    <!--Score wrapper -->
    <div class="w-100 position-absolute score_wrapper ">
        <div class="justify-content-between d-flex score_container text-white">
            <div class="w-50 text-end d-flex flex-row-reverse">
                <div style="width: 150px; margin-right: 15px" class="d-flex justify-content-center">
                    <span class="border-primary score_number  text-center w-100"
                          id="red_score"><?= $match->red_team_score ?></span>
                </div>
            </div>
            <div class="w-50 text-start">
                <div style="width: 150px; margin-left: -13px" class="d-flex justify-content-center">
                    <span class="border-danger  score_number text-center  w-100"
                          id="blue_score"><?= $match->blue_team_score ?></span>
                </div>
            </div>
        </div>
    </div>
    <!--team name wrapper -->
    <div class="team_name_wrapper w-75 position-absolute ">
        <div class="team_name_container border-primary d-flex justify-content-between">
            <div class="text-center " style="margin-left: 210px; width: 160px">
                <span class="text-center"><?= $redTeams->short_name ?></span>
            </div>
            <div class=" text-center" style="width: 175px; margin-right: -45px;">
                <span class="text-center"><?= $blueTeams->short_name ?></span>
            </div>
        </div>
    </div>
    <!--timer wrapper -->
    <div class="timer_wrapper w-100 position-absolute">
        <div class="timer_container  d-flex justify-content-center  text-white">
            <span id="timer_display" class=" me-3">00:00</span>
        </div>
    </div>
    <!--Number of Pole wrapper -->
    <div class="pole_number_wrapper w-100 position-absolute ">
        <div class="pole_number_container border-danger  d-flex justify-content-between text-white">
            <div class=" w-50 text-end ">
                <span id="red_pole" class="margin-end-red-pole"><?= $numberOfRedPole ?></span>
            </div>
            <div class="w-50  text-start">
                <span id="blue_pole" class="margin-start-blue-pole"><?= $numberOfBluePole ?></span>
            </div>
        </div>
    </div>

    <!--Field pattern -->
    <div class="field_pattern_wrapper w-100 position-absolute">
        <div class="field_pattern_container  border-danger d-flex">
            <div class="d-flex flex-column justify-content-between align-items-stretch  border-danger"
                 style="margin-left: 13px">
                <div data-lock-type="<?= $poleTypeCooks['pole_type_3'] ?>" data-pole-number="3"
                     class="ring_pole bg-white" style="margin-top: 10px;"></div>
                <div data-lock-type="<?= $poleTypeCooks['pole_type_2'] ?>" data-pole-number="2"
                     class="ring_pole bg-white" style="margin-top: 5px;"></div>
                <div data-lock-type="<?= $poleTypeCooks['pole_type_1'] ?>" data-pole-number="1"
                     class="ring_pole bg-white" style="margin-bottom: 10px"></div>
            </div>
            <div class="d-flex flex-column justify-content-between align-items-stretch" style="margin-left: 9px">
                <div data-lock-type="<?= $poleTypeCooks['pole_type_5'] ?>" data-pole-number="5"
                     class="ring_pole bg-white" style="margin-top: 34px;"></div>
                <div data-lock-type="<?= $poleTypeCooks['pole_type_4'] ?>" data-pole-number="4"
                     class="ring_pole bg-white" style="margin-bottom: 36px;"></div>
            </div>
            <div class="d-flex flex-column justify-content-center align-items-stretch" style="margin-left: -4px">
                <div data-lock-type="<?= $poleTypeCooks['pole_type_6'] ?>" data-pole-number="6"
                     class="ring_pole bg-white" style="margin-top: -3px;"></div>
            </div>
            <div class="d-flex flex-column justify-content-between align-items-stretch" style="margin-left: -6px">
                <div data-lock-type="<?= $poleTypeCooks['pole_type_8'] ?>" data-pole-number="8"
                     class="ring_pole bg-white" style="margin-top: 34px;"></div>
                <div data-lock-type="<?= $poleTypeCooks['pole_type_7'] ?>" data-pole-number="7"
                     class="ring_pole bg-white" style="margin-bottom: 36px;"></div>
            </div>
            <div class="d-flex flex-column justify-content-between align-items-stretch " style="margin-left: 7px">
                <div data-lock-type="<?= $poleTypeCooks['pole_type_11'] ?>" data-pole-number="11"
                     class="ring_pole bg-white" style="margin-top: 10px;"></div>
                <div data-lock-type="<?= $poleTypeCooks['pole_type_10'] ?>" data-pole-number="10"
                     class="ring_pole bg-white" style="margin-top: 5px;"></div>
                <div data-lock-type="<?= $poleTypeCooks['pole_type_9'] ?>" data-pole-number="9"
                     class="ring_pole bg-white" style="margin-bottom: 10px"></div>
            </div>
        </div>
    </div>
    <!--CHEI - YO slot -->
    <div class="chei_yo_wrapper position-absolute  w-100" style="top: 189px;height: 75px;">
        <div class="chei_yo_container d-none border-primary h-100" style="width: 290px;margin-left: 370px">
            <div
                class="chey_yo_content rounded-3 h-100 shadow position-relative text-center d-flex justify-content-center align-items-center">
                <span class="winner_animation_text fst-italic fw-bold" style="margin-top: -10px">CHEY - YO</span>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script(['pusher.min.js?t=' . time(), 'streaming.js?t=1']);
?>
