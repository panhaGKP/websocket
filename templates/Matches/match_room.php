<?php

/**
 * @var App\Model\Entity\Matche $matche
 * @var integer $numberOfRedRingPoles
 * @var integer $numberOfBlueRingPoles
 * @var integer $redScore
 * @var integer $blueScore
 * @var integer $timerStarted
 * @var \App\View\AppView $this
 */
?>
<?php echo $this->Form->control('timer',[
    'type'=>'hidden',
    'value'=>$matche->timer]) ?>
<?php echo $this->Form->control('timerStart',[
    'type'=>'hidden',
    'value'=>$timerStarted]) ?>
<div class="mt-2 mb-2">
    <h2 class="fs-1 text-center">Room Match</h2>
    <audio id="prepare_sound">
        <source src="/audio/audio_prepare.MP3" type="audio/mpeg">
    </audio>
    <audio id="start_sound">
        <source src="/audio/count1min_audio.MP3" type="audio/mpeg">
    </audio>
    <audio id="finish_sound">
        <source src="/audio/finish_sound.MP3" type="audio/mpeg">
    </audio>
    <audio id="almost_finish_sound">
        <source src="/audio/almost_finish_audio.MP3" type="audio/mpeg">
    </audio>

    <div class="d-none" id="red_first_own" data-red-first-own="0"></div>
    <div class="d-none" id="blue_first_own" data-blue-first-own="0"></div>

    <div id="timer_pause" data-timer-pause="0" class="d-none"></div>
    <h5 class="text-center text-black-50" id="room_code" data-room-code="<?= $matche->room_code ?>">Room code: <?= $matche->room_code ?></h5>
    <?= $this->Html->link('<i class="bi bi-arrows-fullscreen"></i>', '#', ['id' => 'fullscreen-btn', 'escape' => false,'class'=>'rounded-2 mb-2 expand_icon shadow p-2 fs-4 ']) ?>

    <!--new screen-->
    <div class="border bg-white"  id="score_new">
        <div id="score_board" class="d-none border border-2 rounded-3 vh-100 position-relative bg-white d-flex flex-column">
            <?= $this->Html->image('top_background_transparent.svg',['class'=>'position-absolute top-0 start-0 border w-100 z-0']) ?>
            <!--Top title of the score board-->
            <div class="w-100 mt-4 title_of_scor_board">
                <div class="d-flex mx-auto justify-content-center align-items-center ">
                    <?= $this->Html->image('2023_logo_robot_PNG.png',['class'=>'z-1 robocon23_logo']) ?>
                    <p class="title_robocon_in_khmer fs-2 mt-4">ការប្រកួតប្រជែងរ៉ូបូតថ្នាក់ជាតិលើកទី១០ ឆ្នាំ២០២៣</p>
                </div>
            </div>
            <div class="d-flex flex-column  flex-grow-1 p-2 mt-4" style="z-index: 100;">
                <div class="score_board_display_section flex-grow-1 d-flex justify-content-center align-items-center mb-1">
                    <!--score for red team-->
                    <div class="h-100 bg-red w-50 angle_round me-1">
                        <div class="text-center letter_team_name mt-2"><?= $matche->red_team_name ?></div>
                        <div class="bg_of_score_number w-70 mx-auto h-60 d-flex align-items-center justify-content-center">
                            <div id="red_score" class="text-center letter_score"><?= $redScore ?></div>
                        </div>
                    </div>
                    <!--score for blue team-->
                    <div class="h-100 border border-primary bg-bleu w-50 angle_round ms-1">
                        <div class="text-center letter_team_name"><?= $matche->blue_team_name ?></div>
                        <!--                    <div id="blue_score" class="text-center letter_score">--><?php //= $blueScore ?><!--</div>-->
                        <div class="bg_of_score_number w-70 mx-auto h-60 d-flex align-items-center justify-content-center">
                            <div id="blue_score" class="text-center letter_score"><?= $blueScore ?></div>
                        </div>
                    </div>
                </div>
                <div class="below_score_board_section d-flex justify-content-between align-items-center mt-1">
                    <!--number of Ring in red-->
                    <div class="w-25 me-1 bg-red h-100 angle_round d-flex justify-content-center align-items-center">
                        <div id="red_pole" class=" unknown letter_pole text-center d-flex align-items-center justify-content-center">
                            <?= $numberOfRedRingPoles ?>
                        </div>
                    </div>
                    <!--Timer-->
                    <div class="bg-black w-50 h-100 angle_round mx-1 d-flex justify-content-center align-items-center">
                        <div id="timer_display" class="letter_timer text-center text-white d-flex align-items-center justify-content-center">1</div>
                    </div>
                    <!--number of Ring in blue-->
                    <div class="w-25 bg-bleu h-100 angle_round ms-1 d-flex justify-content-center align-items-center">
                        <div id="blue_pole" class="unknown letter_pole text-center justify-content-center align-items-center">
                            <?= $numberOfBlueRingPoles ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chey_yo_modal_bg d-none" id="chey_yo_modal">
                <div class="w-100 h-100">
                    <div class="chey_yo_content rounded-3 w-50 h-25 shadow position-relative text-center d-flex justify-content-center align-items-center">
                        <span class="winner_animation_text fw-bold fst-italic">CHEY - YO</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="countdown_prepare_div" class="border-danger bg-white position-relativee">
            <span id="countdown_prepare" class="countdown-10 letter">10</span>
        </div>
    </div>
</div>
<div id="test" class="d-none border btn btn-secondary">button</div>
<!-- Button trigger modal -->
<!-- Modal -->

<?php
echo $this->Html->script(['pusher.min.js?t='.time(), 'scoreboard.js?t=2']);
?>
