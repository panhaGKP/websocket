<?php

/**
 * @var App\Model\Entity\Matche $matche
 * @var integer $numberOfRedRingPoles
 * @var integer $numberOfBlueRingPoles
 * @var integer $redScore
 * @var integer $blueScore
 * @var integer $timerStarted
 * @var \App\View\AppView $this
 * @var App\Model\Entity\Team $redTeam
 * @var App\Model\Entity\Team $blueTeam
 * @var array $poleTypeCooks
 * @var boolean $isCheyyoEnable
 */
?>
<?php echo $this->Form->control('timer',[
    'type'=>'hidden',
    'value'=>$matche->timer]) ?>
<?php echo $this->Form->control('timerStart',[
    'type'=>'hidden',
    'value'=>$timerStarted]) ?>



<div class="mt-2 mb-2">
    <h2 class="fs-1 text-center">Room Match</h2>
    <audio id="prepare_sound">
        <source src="/audio/audio_prepare.MP3" type="audio/mpeg">
    </audio>
    <audio id="start_sound">
        <source src="/audio/count1min_audio.MP3" type="audio/mpeg">
    </audio>
    <audio id="finish_sound">
        <source src="/audio/finish_sound.MP3" type="audio/mpeg">
    </audio>
    <audio id="almost_finish_sound">
        <source src="/audio/almost_finish_audio.MP3" type="audio/mpeg">
    </audio>

    <div class="d-none" id="red_first_own" data-red-first-own="0"></div>
    <div class="d-none" id="blue_first_own" data-blue-first-own="0"></div>

    <div id="timer_pause" data-timer-pause="0" class="d-none"></div>
    <div class="d-none" id="is_cheyyo_enable" data-cheyyo-enable="<?= $isCheyyoEnable ?>"></div>

    <h5 class="text-center text-black-50" id="room_code" data-room-code="<?= $matche->room_code ?>">Room code: <?= $matche->room_code ?></h5>
    <?= $this->Html->link('<i class="bi bi-arrows-fullscreen"></i>', '#', ['id' => 'fullscreen-btn', 'escape' => false,'class'=>'rounded-2 mb-2 expand_icon shadow p-2 fs-4 ']) ?>

    <!--new screen-->
    <div class="border bg-white"  id="score_new">
        <div id="score_board" class="d-none border border-2 rounded-3 vh-100 position-relative bg-white d-flex flex-column">
            <!--Top title of the score board-->
            <div class="w-100 d-none">
                <div class="title_of_score_board position-absolute mx-auto justify-content-center align-items-center border border-primary">
                    <p class="title_robocon_in_khmer fs-2 mt-4">ការប្រកួតប្រជែងរ៉ូប៉ូតអាស៊ីប៉ាស៊ីហ្វិកលើកទី២២ ឆ្នាំ២០២៣</p>
                </div>
            </div>
            <?= $this->Html->image('LED_display.jpg',['class'=>'','style'=>'margin-top:-20px']) ?>
            <div class="close_point_letter position-absolute w-100 d-flex justify-content-between">
                <div class="close_red"></div>
                <div class="close_blue"></div>
            </div>
            <!--Row of Red Country, Red flag - Blue flg, Blue country-->
            <div class=" border-primary position-absolute w-100 team_name_row">
                <div class=" border-danger d-flex justify-content-center">
                    <!--Red side -->
                    <div class="w-50 d-flex  border-primary">
                        <div class=" border-warning d-flex" style="width: 75%">
                            <div class="d-flex align-items-center  mx-auto">
                                <div class="text-center letter_team_name" style="text-transform: uppercase; margin-top: -15px"><?= $matche->red_team_name ?></div>
                                <?php
                                    $cssCountryRed = strtolower($redTeam->name) == 'nepal' ? 'country_flag': 'country_flag_with_border';
                                ?>
                                <div class=" border-warning d-flex justify-content-center align-items-center">
                                    <?= $this->Html->image($redTeam->image_path,['class'=>$cssCountryRed]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Blue side -->
                    <div class="w-50  border-primary d-flex flex-row-reverse">
                        <div class="d-flex  border-danger d-flex" style="width: 77%">
                            <div class="d-flex align-items-center mx-auto">
                                <div class=" border-warning d-flex justify-content-center align-items-center">
                                    <?php
                                        $cssCountryBlue = strtolower($blueTeam->name) == 'nepal' ? 'country_flag': 'country_flag_with_border';
                                    ?>
                                    <?= $this->Html->image($blueTeam->image_path,['class'=>$cssCountryBlue]) ?>
                                </div>
                                <div class="text-center letter_team_name " style="text-transform: uppercase; margin-top: -15px"><?= $matche->blue_team_name ?></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Score Row-->
            <div class="border-primary position-absolute w-100 team_score_row d-flex">
                <div class="letter_score w-50  border-warning d-flex">
                    <span  id="red_score" class=" border-warning d-block text-center" style="width: 60%;margin-left: 20px">
                        <?= $redScore ?>
                    </span>
                </div>
                <div id="" class="letter_score w-50 border-warning d-flex flex-row-reverse">
                    <span  id="blue_score" class=" border-warning text-center ms-20" style="width:60%;margin-right: 70px;">
                        <?= $blueScore ?>
                    </span>
                </div>
            </div>
            <!--Number of Ring -->
            <div class="border border-primary position-absolute w-100 team_pole_row d-none">
                <div class="d-flex">
                    <div id="red_pole" class="text-white border border-warning w-50 unknown letter_pole text-end pealign-items-center pe-20 justify-content-center">
                        <?= $numberOfRedRingPoles ?>
                    </div>
                    <div id="blue_pole" class="text-white border border-warning w-50 unknown letter_pole ps-20 justify-content-center align-items-center">
                        <?= $numberOfBlueRingPoles ?>
                    </div>
                </div>
            </div>
            <!--Timer and pattern field-->
            <div class="position-absolute w-100 timer_row  border-primary">
                <div class="mx-auto  border-danger d-flex justify-content-center" style="">
                    <div class="d-flex align-items-center position-relative w-75  border-danger flex-row-reverse">
                        <div class="position-absolute" style="top:60px; left:160px">
                            <?= $this->Html->image('put_to_score_board.svg',['class'=>'field-pattern']) ?>
                        </div>
                        <div style="margin-right: 30px; width: 60%" id="timer_display" class="ms-5  letter_timer text-center d-flex align-items-center justify-content-center">1</div>
                        <!--Field pattern -->
                        <div class="field_pattern_wrapper w-100 position-absolute  border-primary">
                            <div class="field_pattern_container d-flex ">
                                <div class="d-flex flex-column justify-content-between align-items-stretch" style="margin-left: 13px">
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_3'] ?>" data-pole-number="3" class="ring_pole bg-white" style="margin-top: 12px;"></div>
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_2'] ?>" data-pole-number="2" class="ring_pole bg-white" style="margin-top: 2px;"></div>
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_1'] ?>" data-pole-number="1" class="ring_pole bg-white" style="margin-bottom: 15px"></div>
                                </div>
                                <div class="d-flex flex-column justify-content-between align-items-stretch" style="margin-left: 9px">
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_5'] ?>" data-pole-number="5" class="ring_pole bg-white" style="margin-top: 34px;"></div>
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_4'] ?>" data-pole-number="4" class="ring_pole bg-white" style="margin-bottom: 43px;"></div>
                                </div>
                                <div class="d-flex flex-column justify-content-center align-items-stretch" style="margin-left: -3px">
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_6'] ?>" data-pole-number="6" class="ring_pole bg-white" style="margin-top: -4px;"></div>
                                </div>
                                <div class="d-flex flex-column justify-content-between align-items-stretch" style="margin-left: -5px">
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_8'] ?>" data-pole-number="8" class="ring_pole bg-white" style="margin-top: 35px;"></div>
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_7'] ?>" data-pole-number="7" class="ring_pole bg-white" style="margin-bottom: 40px;"></div>
                                </div>
                                <div class="d-flex flex-column justify-content-between align-items-stretch " style="margin-left: 10px">
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_11'] ?>" data-pole-number="11" class="ring_pole bg-white" style="margin-top: 13px;"></div>
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_10'] ?>" data-pole-number="10" class="ring_pole bg-white" style="margin-top: -1px;"></div>
                                    <div data-lock-type="<?= $poleTypeCooks['pole_type_9'] ?>" data-pole-number="9" class="ring_pole bg-white" style="margin-bottom: 14px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- CHEY-YO modal -->
            <div class="chey_yo_modal_bg d-none" id="chey_yo_modal">
                <div class="w-100 h-100">
                    <div class="chey_yo_content rounded-3 w-50 h-25 shadow position-relative text-center d-flex justify-content-center align-items-center">
                        <span class="winner_animation_text fw-bold fst-italic" style="font-size: 130px">CHEY - YO</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="countdown_prepare_div" class="border-danger border bg-white position-relative vh-100">
            <span id="countdown_prepare" class="countdown-10 letter">10</span>
        </div>
    </div>
</div>
<div id="test" class="d-none border btn btn-secondary">button</div>
<!-- Button trigger modal -->
<!-- Modal -->

<?php
echo $this->Html->script(['pusher.min.js?t='.time(), 'scoreboard.js?t=2']);
?>
