<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Matche> $matches
 * @var array $options
 */
$value = $this->request->getQuery('note')== null? '': $options[$this->request->getQuery('note')];
?>
<div class="matches index content mt-3 px-5">

    <div class="d-flex justify-content-between">
        <h3><?= __('Matches') ?></h3>
        <div>
            <?= $this->Html->link(__('Score operation Room'), ['action' => 'enteringRoom'], ['class' => 'btn btn-warning shadow-sm','target'=>'_blank']) ?>
            <?= $this->Html->link('<i class="bi bi-plus-circle me-1"></i>New Match', ['action' => 'add'], ['class' => 'btn btn-primary shadow-sm','escape'=>false]) ?>
        </div>
    </div>
    <div class="d-flex">
        <?= $this->Form->create(null,['url'=>['action'=>'index'],'type'=>'get','class'=>'d-flex']) ?>
       <div>
           <?php
           echo $this->Form->select(
               'note',  // Replace with the actual field name in your database table
               $options,      // An array of options for the select input
               ['empty' => 'Select an option','value'=> $value] // Additional attributes/options
           );
           ?>
       </div>
        <div>
            <?= $this->Form->submit('Filter',['class'=>'btn btn-primary ms-1']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr class="">
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th>Room <br>Code</th>
                    <th>Blue</th>
                    <th>Red</th>
                    <th>Timer</th>
                    <th>Note</th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($matches as $match): ?>
                <tr>
                    <td><?= $this->Number->format($match->id) ?></td>
                    <td><?= $this->Number->format($match->room_code) ?></td>

                    <td class="red-text">
                        <?= h($match->red_team_name) ?>
                        <br>
                        <?= $this->Number->format($match->red_team_score) ?>
                    </td>
                    <td class="blue-text">
                        <?= h($match->blue_team_name) ?>
                        <br>
                        <?= $this->Number->format($match->blue_team_score) ?>
                    </td>
                    <td><?= $this->Number->format($match->timer) ?></td>
                    <td><?= $match->note ?></td>
                    <td><?= h($match->created) ?></td>
                    <td class="actions">
                        <?= $this->Html->link('Red operate score<i class="ms-1 bi bi-arrow-right-circle"></i>',['action' => 'redScoreOperation', $match->room_code],['class'=>'btn btn-danger', 'escape'=>false,'target'=>'_blank']) ?>
                        <?= $this->Html->link('Blue operate score<i class="ms-1 bi bi-arrow-right-circle"></i>',['action' => 'blueScoreOperation', $match->room_code],['class'=>'btn btn-primary', 'escape'=>false,'target'=>'_blank']) ?>
                        <?= $this->Html->link('Match<i class="ms-1 bi bi-arrow-right-circle"></i>',['action' => 'matchRoom', $match->room_code],['class'=>'btn btn-purple', 'escape'=>false,'target'=>'_blank']) ?>
                        <?= $this->Html->link('Stream<i class="ms-1 bi bi-arrow-right-circle"></i>',['action' => 'streaming', $match->room_code],['class'=>'btn btn-primary', 'escape'=>false,'target'=>'_blank']) ?>
                        <?= $this->Html->link('Offline<i class="ms-1 bi bi-arrow-right-circle"></i>',['controller'=>'OfflineMatches','action' => 'offlineMatchRoom', $match->room_code],['class'=>'btn btn-warning d-none', 'escape'=>false,'target'=>'_blank']) ?>
                        <?= $this->Html->link('<i class="bi bi-pencil-square"></i>', ['action' => 'edit', $match->id],['class'=>'btn btn-outline-warning','escape'=>false]) ?>
                        <?= $this->Form->postLink('<i class="bi bi-trash"></i>', ['action' => 'delete', $match->id], ['confirm' => __('Are you sure you want to delete # {0}?', $match->id),'class'=>'btn btn-outline-danger','escape'=>false]) ?>

                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
