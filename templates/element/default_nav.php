<?php
/**
 * @var \App\View\AppView $this
 */
$controller = $this->request->getParam('controller');
//debug($controller == 'Matches')
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm">
    <div class="container-fluid container">
        <a class="navbar-brand" href="#"><?= $this->Html->image('Robocon_logo.png',['style'=>'width:100px;']) ?></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link rounded-3 text-center me-md-2 me-sm-0 <?= $controller == 'Matches'? 'current':'' ?>" aria-current="page" href="<?= $this->Url->build(['controller' => 'Matches', 'action' => 'index']) ?>"><i class="bi bi-battery-charging me-1"></i>Matches</a>
                <a class="nav-link rounded-3 text-center <?= $controller == 'Teams'? 'current':'' ?>" href="<?= $this->Url->build(['controller'=>'Teams','action'=>'index']) ?>"><i class="bi bi-people-fill me-1"></i>Teams</a>
            </div>
        </div>
    </div>
</nav>
