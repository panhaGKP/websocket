<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Team $team
 */
?>
<div class="row mt-3 container">

    <div class="column-responsive column-80">
        <div class="teams form content">
            <?= $this->Form->create($team,['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend class="fs-1"><?= __('Edit Team') ?></legend>
                <?php
                echo $this->Form->control('name',['label'=>'Team name', 'class'=>'w-50']);
                echo $this->Form->control('short_name',['label'=>'Team short_name', 'class'=>'w-50']);
                echo $this->Form->control('team_image',['label'=>'Team image', 'class'=>'w-50', 'type' => 'file']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Save Change'),['class'=>'btn btn-warning']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
