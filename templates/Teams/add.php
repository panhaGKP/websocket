<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Team $team
 */
?>
<div class="row mt-3 container">

    <div class="column-responsive column-80">
        <div class="teams form content">
            <?= $this->Form->create($team, ['enctype' => 'multipart/form-data']) ?>
            <fieldset>
                <legend class="fs-1"><?= __('Add Team') ?></legend>
                <?php
                    echo $this->Form->control('name',['label'=>'Team name','placeholder'=>'i.e. CAMBODIA', 'class'=>'w-50']);
                    echo $this->Form->control('short_name',['label'=>'Team short name','placeholder'=>'i.e. CAM', 'class'=>'w-50']);
                    echo $this->Form->control('team_image',['label'=>'Team image', 'class'=>'w-50', 'type' => 'file']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit'),['class'=>'btn btn-success']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
