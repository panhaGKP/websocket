<?php

namespace App\Controller;

use Cake\Http\Client;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;

class StreamsController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['testScore','roboconGameOnLive']);
    }
    public function testScore(){
        $this->autoRender = false;
        $file = WWW_ROOT . 'stream' . DS . 'teststream.txt';
        $content = file_get_contents($file);
        // debug($content);
        // Do something with the file content

        $http = new Client();
        $response = $http->get('https://stg.roboconscoring.com/streams/testScore');
        $body = $response->getBody();
//        debug($body);
        echo $body;
    }

    /**
     * @return Response
     */
    public function roboconGameOnLive()
    {
        $jsonFilePath = WWW_ROOT . 'stream/myJsonFile.json';

        if (file_exists($jsonFilePath)) {
            $response = new Response();
            $response = $response->withType('json');
            $response = $response->withFile($jsonFilePath);

            // Optionally, you can set additional response headers here.
            // For example, to force a download:
//             $response = $response->withDownload('myfile.json');

            return $response;
        } else {
            throw new NotFoundException(__('JSON file not found.'));
        }
    }
}
