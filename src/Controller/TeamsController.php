<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Teams Controller
 *
 * @property \App\Model\Table\TeamsTable $Teams
 * @method \App\Model\Entity\Team[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TeamsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $teams = $this->paginate($this->Teams);

        $this->set(compact('teams'));
    }

    /**
     * View method
     *
     * @param string|null $id Team id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $team = $this->Teams->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('team'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $team = $this->Teams->newEmptyEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            //dd(($data['team_image'])->getClientFilename());
            $team = $this->Teams->patchEntity($team, $data);
            if (!$team->getErrors() && !empty($data['team_image'])) {
                $this->_uploadPicture($team);
            }
            if ($this->Teams->save($team)) {
                $this->Flash->success(__('The team has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The team could not be saved. Please, try again.'));
        }
        $this->set(compact('team'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Team id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $team = $this->Teams->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $team = $this->Teams->patchEntity($team,$data);
            try {
                if (!$team->getErrors() && !empty($data['team_image'])) {
                    $this->_uploadPicture($team);
                }
            }catch (\Exception $e){
                // do sth
                echo $e->getMessage();
            }
            if ($this->Teams->save($team)) {
                $this->Flash->success(__('The team has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The team could not be saved. Please, try again.'));
        }
        $this->set(compact('team'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Team id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $team = $this->Teams->get($id);
        if ($this->Teams->delete($team)) {
            $this->Flash->success(__('The team has been deleted.'));
        } else {
            $this->Flash->error(__('The team could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    protected function _uploadPicture(&$team)
    {
        $pictureUploaded = $this->getRequest()->getData('team_image');
        $tmpFilePath = $pictureUploaded->getStream()->getMetadata('uri');
//        dd($tmpFilePath);
        if ($team->isNew()) {
            $team->image_path = null;
        }
        //debug($pictureUploaded->getClientMediaType());
//        dd(!is_array($pictureUploaded) && $pictureUploaded->getError() === UPLOAD_ERR_OK && ($pictureUploaded->getClientMediaType() === 'image/jpeg' || $pictureUploaded->getClientMediaType() === 'image/png'));
        if (!is_array($pictureUploaded) && $pictureUploaded->getError() === UPLOAD_ERR_OK && ($pictureUploaded->getClientMediaType() === 'image/jpeg' || $pictureUploaded->getClientMediaType() === 'image/png')) {
            // todo for security reason, please check the file type by system not from the file user uploaded by using function get file info
            $uploadDir = 'uploads' . DS . 'team_images' . DS;
            if (!is_dir(WWW_ROOT . 'img' . DS . $uploadDir)) {
                @mkdir(WWW_ROOT . 'img' . DS . $uploadDir, 0775, true);
            }
            $filePicName = uniqid() . '.png';
            $targetPath = WWW_ROOT . 'img' . DS . $uploadDir . $filePicName;
            @move_uploaded_file($tmpFilePath, $targetPath);
            $team->image_path = str_replace(DS, '/', $uploadDir) . $filePicName;
        }
    }
}
