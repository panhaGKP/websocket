<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Matches Model
 *
 * @method \App\Model\Entity\Matche newEmptyEntity()
 * @method \App\Model\Entity\Matche newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Matche[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Matche get($primaryKey, $options = [])
 * @method \App\Model\Entity\Matche findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Matche patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Matche[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Matche|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Matche saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Matche[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Matche[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Matche[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Matche[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MatchesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('matches');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        // in the initialize() method
        $this->addBehavior('Muffin/Trash.Trash');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('room_code')
            ->requirePresence('room_code', 'create')
            ->notEmptyString('room_code');

        $validator
            ->integer('blue_team_id')
            ->requirePresence('blue_team_id', 'create')
            ->notEmptyString('blue_team_id');

        $validator
            ->scalar('blue_team_name')
            ->maxLength('blue_team_name', 255)
            ->allowEmptyString('blue_team_name');

        $validator
            ->integer('blue_team_score')
            ->notEmptyString('blue_team_score');

        $validator
            ->integer('red_team_id')
            ->requirePresence('red_team_id', 'create')
            ->notEmptyString('red_team_id');

        $validator
            ->scalar('red_team_name')
            ->maxLength('red_team_name', 255)
            ->allowEmptyString('red_team_name');

        $validator
            ->integer('red_team_score')
            ->notEmptyString('red_team_score');

        $validator
            ->integer('timer')
            ->notEmptyString('timer');

        return $validator;
    }
}
