<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ScorePoles Model
 *
 * @property \App\Model\Table\MatchesTable&\Cake\ORM\Association\BelongsTo $Matches
 *
 * @method \App\Model\Entity\ScorePole newEmptyEntity()
 * @method \App\Model\Entity\ScorePole newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ScorePole[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ScorePole get($primaryKey, $options = [])
 * @method \App\Model\Entity\ScorePole findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ScorePole patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ScorePole[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ScorePole|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ScorePole saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ScorePole[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ScorePole[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ScorePole[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ScorePole[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ScorePolesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('score_poles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Matches', [
            'foreignKey' => 'matche_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('matche_id')
            ->notEmptyString('matche_id');

        $validator
            ->integer('room_code')
            ->requirePresence('room_code', 'create')
            ->notEmptyString('room_code');

        $validator
            ->integer('pole_number')
            ->requirePresence('pole_number', 'create')
            ->notEmptyString('pole_number');

        $validator
            ->integer('red_score')
            ->requirePresence('red_score', 'create')
            ->notEmptyString('red_score');

        $validator
            ->boolean('pole_to_red')
            ->notEmptyString('pole_to_red');

        $validator
            ->integer('blue_score')
            ->requirePresence('blue_score', 'create')
            ->notEmptyString('blue_score');

        $validator
            ->boolean('pole_to_blue')
            ->notEmptyString('pole_to_blue');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('matche_id', 'Matches'), ['errorField' => 'matche_id']);

        return $rules;
    }
}
