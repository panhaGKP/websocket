<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ScorePole Entity
 *
 * @property int $id
 * @property int $matche_id
 * @property int $room_code
 * @property int $pole_number
 * @property int $red_score
 * @property bool $pole_to_red
 * @property int $blue_score
 * @property bool $pole_to_blue
 * @property int $timestamp_click_red
 * @property int $timestamp_click_blue
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Match $match
 */
class ScorePole extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'matche_id' => true,
        'room_code' => true,
        'pole_number' => true,
        'red_score' => true,
        'pole_to_red' => true,
        'timestamp_click_red' => true,
        'blue_score' => true,
        'pole_to_blue' => true,
        'timestamp_click_blue' => true,
        'created' => true,
        'modified' => true,
        'match' => true,
    ];
}
