<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Matche Entity
 *
 * @property int $id
 * @property int $room_code
 * @property int $blue_team_id
 * @property string|null $blue_team_name
 * @property int $blue_team_score
 * @property int $red_team_id
 * @property string|null $red_team_name
 * @property int $red_team_score
 * @property int $timer
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $red_first_own_pole_number
 * @property string $red_first_own_pole_at
 * @property int $blue_first_own_pole_number
 * @property string $blue_first_own_pole_at
 * @property string $note
 */
class Matche extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'room_code' => true,
        'blue_team_id' => true,
        'blue_team_name' => true,
        'blue_team_score' => true,
        'red_team_id' => true,
        'red_team_name' => true,
        'red_team_score' => true,
        'timer' => true,
        'created' => true,
        'modified' => true,
        'note' => true,
        'red_first_own_pole_number' => true,
        'red_first_own_pole_at' => true,
        'blue_first_own_pole_number' => true,
        'blue_first_own_pole_at' => true,
    ];
}
