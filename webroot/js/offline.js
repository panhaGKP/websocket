$(document).ready(function () {

    let roomCode = $('#room_code').data('room-code');
    let timerStart = $('#timerstart');

    let audioPrepare = $('#prepare_sound');
    let audioStart = $('#start_sound');
    let audioFinish = $('#finish_sound');
    let audioAlmostFinish = $('#almost_finish_sound');

    let $timerDisplay = $('#timer_display');
    let $timeValue = $('#timer');
    $timerDisplay.text('0'+ $timeValue.val()+':00')
    //$('#chey_yo_modal').removeClass('d-none')

    $('#chey_yo_modal').on('click',function (){
        $(this).addClass('d-none')
    })

    $('#start-timer').on('click',function () {
        console.log('start time started');
        let roomCode = $('#room_code').data('room-code');
        let timerStart = $('#timerstart');

        console.log(roomCode)
        let xhr = $.ajax({
            url:"/offlineMatches/startTimer",
            method: "post",
            dataType:"json",
            data:{
                roomCode:roomCode,
            }
        })
        xhr.done(function (response) {
            //console.log('hi')
            console.log(response)
            if (roomCode === response.data.roomCode) {
                //console.log(response)
                timerStart.val(response.data.timerStarted);
            }
        })
    })
    applyBgToLockButton()
    //disableCountDownPrepareDisplay()
    performTimerStart()
    //circle click score
    $('.circle').on('click', function () {
        /*==Change color =============*/
        let parentDiv = $(this).parent();
        console.log('circle click');
        let color1 = $(this).data('color');
        // console.log('this click :'+color1);
        $(this).addClass(color1);
        let $otherCircle = parentDiv.find('.circle').not(this);
        let color2 = $otherCircle.data('color');
        // console.log('other color :'+color2);
        $otherCircle.removeClass(color2);

        //lock value
        let thisLock = $(this).data('lock');
        let otherLock = $otherCircle.data('lock')

        /*====Request update score=======*/
        let roomCode = $('#room_code').data('room-code');
        let poleNumber = parentDiv.data('pole-number');
        let score = $(this).data('score');
        let teamColor = $(this).data('color');
        // console.log(poleNumber === 1);
        if (thisLock) {
            console.log('lock request');

            return;
        }
        if (poleNumber === 1 || poleNumber === 2 || poleNumber === 3) {
            let xhr1 = $.ajax({
                method: 'post',
                url: '/offlineMatches/scoreForPole123',
                dataType: 'json',
                data: {
                    pole_number : poleNumber,
                    team_color : teamColor,
                    score: score,
                    room_code : roomCode,
                }
            })
            xhr1.done(function (response) {
                console.log('done update pole ' + response.poleNumber)
                updateScoreBoard(response.data)
            })
        } else if (poleNumber === 6 || poleNumber === 5 || poleNumber === 4 || poleNumber === 7 || poleNumber === 8 ) {
            let xhr1 = $.ajax({
                method: 'post',
                url: '/offlineMatches/scoreForPole45678',
                dataType: 'json',
                data: {
                    pole_number : poleNumber,
                    team_color : teamColor,
                    score: score,
                    room_code : roomCode,
                }
            })
            xhr1.done(function (response) {
                console.log('done update pole ' + response.poleNumber)
                updateScoreBoard(response.data)
            })
        } else {
            let xhr1 = $.ajax({
                method: 'post',
                url: '/offlineMatches/scoreForPole91011',
                dataType: 'json',
                data: {
                    pole_number : poleNumber,
                    team_color : teamColor,
                    score: score,
                    room_code : roomCode,
                }
            })
            xhr1.done(function (response) {
                console.log('done update pole ' + response.poleNumber)
                updateScoreBoard(response.data)
            })
        }

        //lock yourself and unlock request for other
        $(this).data('lock',1);
        $otherCircle.data('lock',0)
        /*console.log('this lock: '+thisLock)
        console.log('other Lock: '+otherLock)*/
    })
    $('#reset-score').on('click', function (){
        $('.confirm-letter').addClass('d-none');
        $('#loading-confirm').removeClass('d-none')
        $(this).addClass('disabled');
        console.log(roomCode)
        let xhr1 = $.ajax({
            method: 'get',
            url: '/offlineMatches/resetScoreMatch/'+roomCode,
            dataType: 'json',
            data: {}
        })
        xhr1.done(function (response) {
            console.log(parseInt(response.roomCode) !== roomCode)
            if(parseInt(response.roomCode) !== roomCode){
                return;
            }
            if(parseInt(response.code) === 200){
                let $circle = $('.circle');
                //reset lock
                $circle.data('lock', 0);
                //reset bg
                $circle.removeClass('red blue');
                $('#resetScoreModal').modal('hide');

                $('.confirm-letter').removeClass('d-none');
                $('#loading-confirm').addClass('d-none')
                $('#reset-score').removeClass('disabled');
                updateScoreBoard(response.data)
            }

        })
    })
    function disableCountDownPrepareDisplay()
    {
        $('#score_board').removeClass('d-none');
        $('#countdown_prepare_div').addClass('d-none');
    }
    function displayCountdownPrepare(countdown)
    {
        $('#countdown_prepare').text(countdown)
    }
    function displayCountdown(countdown)
    {
        if (countdown === 10) {
            audioAlmostFinish.get(0).play()
        }
        let minutes = Math.floor(countdown / 60);
        let seconds = countdown % 60;
        let formattedSeconds = seconds < 10 ? '0' + seconds : seconds;
        $('#timer_display').text('0' + minutes + ':' + formattedSeconds);
    }
    function performTimerStart()
    {
        let value = timerStart.val();

        let checkChangeAndStartTimer = function () {
            let latestValue = timerStart.val();

            if (latestValue !== value) {
                console.log('perform start timer')
                let countdownInterval;
                let minutes = $('#timer').val(); // Set the countdown time in minutes
                let countdown = minutes * 60 + 11; // Convert minutes to seconds // plus 1 because delay 1
                let initialCountDown = countdown;
                // let noPlay = 0;
                let countdownIntervalPrepare;
                let countdownPrepare = parseInt($('#countdown_prepare').text());
                audioPrepare.get(0).play();
                //Display prepare countdown
                displayCountdownPrepare(countdownPrepare);

                //start the countdown prepare
                countdownIntervalPrepare = setInterval(function () {
                    //play the sound alert
                    // console.log($('#prepare_sound').attr('src'));
                    countdownPrepare--;
                    displayCountdownPrepare(countdownPrepare);
                    // Countdown is finished
                    if (countdownPrepare === -1) {
                        clearInterval(countdownIntervalPrepare);
                        $('#countdown_prepare_div').addClass('d-none');
                        $('#score_board').removeClass('d-none');
                    }
                }, 1000);

                // Display the initial countdown time
                displayCountdown(countdown);

                // Start the countdown
                countdownInterval = setInterval(function () {

                    countdown--;
                    if (countdown > 0 && countdown % 60 === 0 && countdown < (initialCountDown - 11)) {
                        // noPlay = 1
                        audioStart.get(0).play()
                    }

                    displayCountdown(countdown);
                    let timerPause = $('#timer_pause').data('timer-pause')
                    if (timerPause) {
                        clearInterval(countdownInterval);
                    }
                    /*if(countdown === 10){
                        audioAlmostFinish.get(0).play()
                    }*/
                    // Countdown is finished
                    if (countdown === 0) {
                        audioFinish.get(0).play()
                        clearInterval(countdownInterval);
                    }
                }, 1000);
            } else {
                console.log('still check timer');
                setTimeout(checkChangeAndStartTimer,400); // check every 2s, change here if we wish to check faster
            }
        }
        checkChangeAndStartTimer();
    }
    function updateScoreBoard(data)
    {
        let nRedPoles = data.number_of_red_pole;
        let nBluePoles = data.number_of_blue_pole;
        let redFirstOwnParent = $('#red_first_own');
        let blueFirstOwnParent = $('#blue_first_own')
        let redFirstOwn = redFirstOwnParent.data('red-first-own');
        let blueFirstOwn = blueFirstOwnParent.data('blue-first-own');
        let timer = $('#timer_display').text();
        // console.log(timer)
        // console.log(redFirstOwn +' : '+blueFirstOwn)

        if (data.room_code === roomCode) {
            $('#red_score').text(data.red_score);
            $('#blue_score').text(data.blue_score);
            $('#red_pole').text(data.number_of_red_pole);
            $('#blue_pole').text(data.number_of_blue_pole);

            if (data.recent !== undefined) {
                let teamColor = data.recent.team_color;
                if (!redFirstOwn && teamColor === 'red') {
                    redFirstOwnParent.data('red-first-own', 1)
                    let xhr = $.ajax({
                        method: 'post',
                        url: '/offlineMatches/firstOwnPole',
                        dataType: 'json',
                        data: {
                            room_code: roomCode,
                            timer: timer,
                            team_color: data.recent.team_color,
                            pole_number: data.recent.pole_number,
                        }
                    })
                    xhr.done(function (response) {
                        console.log(response.message)
                    })
                }
                if (!blueFirstOwn && teamColor === 'blue') {
                    blueFirstOwnParent.data('blue-first-own', 1)
                    let xhr = $.ajax({
                        method: 'post',
                        url: '/matches/firstOwnPole',
                        dataType: 'json',
                        data: {
                            room_code: roomCode,
                            timer: timer,
                            team_color: data.recent.team_color,
                            pole_number: data.recent.pole_number,
                        }
                    })
                    xhr.done(function (response) {
                        console.log(response.message)
                    })
                }
            }

            // console.log(data.recent.pole_number)
            // console.log(data.recent.team_color)
        }
        let $cheyYoWrapper = $('#chey_yo_modal');
        let $cheyYo = $('.chey_yo_content');
        let $dataTimerPause = $('#timer_pause');


        // Chey yo part
        console.log(nRedPoles);
        if (nRedPoles === 8) {
            console.log('popup')
            $cheyYo.addClass('bg-red')
            $cheyYoWrapper.removeClass('d-none')
            $dataTimerPause.data('timer-pause', 1);
        }
        if (nBluePoles === 8) {
            $cheyYo.addClass('bg-bleu')
            $cheyYoWrapper.removeClass('d-none')
            $dataTimerPause.data('timer-pause', 1);
        }
    }
    function applyBgToLockButton()
    {
        // console.log('hello')
        let $this = $('.circle');
        $this.each(function () {
            let bgColorLock = $(this).data('color');
            let isLock = $(this).data('lock');
            if (isLock) {
                $(this).addClass(bgColorLock)
            }
        })
    }
})
