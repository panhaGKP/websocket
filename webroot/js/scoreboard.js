$(document).ready(function () {
    let roomCode = $("#room_code").data("room-code");
    let isCheyyoEnable = $('#is_cheyyo_enable').data('cheyyo-enable');
    console.log(isCheyyoEnable)
    if (isCheyyoEnable) {
        console.log('cheyyo enable')
    } else {
        console.log('cheyyo disable')
    }

    $("#chey_yo_modal").on("click", function () {
        $(this).addClass("d-none");
    });
    $("#chey_yo_popup").modal("show");
    let audioPrepare = $("#prepare_sound");
    let audioStart = $("#start_sound");
    let audioFinish = $("#finish_sound");
    let audioAlmostFinish = $("#almost_finish_sound");

    let $fullScreenBtn = $("#fullscreen-btn");
    let $timerDisplay = $("#timer_display");
    let $timeValue = $("#timer");
    $timerDisplay.text("0" + $timeValue.val() + ":00");
    $fullScreenBtn.on("click", function () {
        let score = document.getElementById("score_new");
        if (!document.fullscreenElement) {
            score.requestFullscreen();
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
            $(this).removeClass("d-none");
        }
    });
    // let audio = document.getElementById('prepare_sound');
    let timerStart = $("#timerstart");

    $("#test").on("click", function () {
        audioPrepare.get(0).play();
    });
    // Set up Pusher
    let pusher = new Pusher("1eaf2f48999ec6fdee2b", {
        //YOUR_APP_KEY
        cluster: "ap1", // YOUR_APP_CLUSTER
        encrypted: true,
    });
    let connection = pusher.subscribe("testConnection");
    connection.bind("test", function (data) {
        console.log(data);
        let isConnected = 0;
        if (data.room_code === roomCode) {
            isConnected = 1;
        }
        let xhr = $.ajax({
            method: "post",
            url: "/matches/broadcastRespondConnection",
            dataType: "json",
            data: {
                room_code: roomCode,
                connection: isConnected,
                connection_red: data.connection_red,
                connection_blue: data.connection_blue,
            },
        });
    });

    let channelTimer = pusher.subscribe("timer");
    channelTimer.bind("update", function (data) {
        // console.log('hello')
        // console.log(data)
        // console.log(roomCode)
        if (roomCode === data.roomCode) {
            timerStart.val(data.timerStarted);
        }
    });

    let channelMatchScoreAndPole = pusher.subscribe("matchScoreAndPole");
    channelMatchScoreAndPole.bind("update", function (data) {
        let nRedPoles = data.number_of_red_pole;
        let nBluePoles = data.number_of_blue_pole;
        let redFirstOwnParent = $("#red_first_own");
        let blueFirstOwnParent = $("#blue_first_own");
        let redFirstOwn = redFirstOwnParent.data("red-first-own");
        let blueFirstOwn = blueFirstOwnParent.data("blue-first-own");
        let timer = $("#timer_display").text();
        // console.log(timer)
        // console.log(redFirstOwn +' : '+blueFirstOwn)

        if (data.room_code === roomCode) {
            $("#red_score").text(data.red_score);
            $("#blue_score").text(data.blue_score);
            $("#red_pole").text(data.number_of_red_pole);
            $("#blue_pole").text(data.number_of_blue_pole);

            if (data.recent !== undefined) {
                let teamColor = data.recent.team_color;
                if (!redFirstOwn && teamColor === "red") {
                    redFirstOwnParent.data("red-first-own", 1);
                    let xhr = $.ajax({
                        method: "post",
                        url: "/matches/firstOwnPole",
                        dataType: "json",
                        data: {
                            room_code: roomCode,
                            timer: timer,
                            team_color: data.recent.team_color,
                            pole_number: data.recent.pole_number,
                        },
                    });
                    xhr.done(function (response) {
                        console.log(response.message);
                    });
                }
                if (!blueFirstOwn && teamColor === "blue") {
                    blueFirstOwnParent.data("blue-first-own", 1);
                    let xhr = $.ajax({
                        method: "post",
                        url: "/matches/firstOwnPole",
                        dataType: "json",
                        data: {
                            room_code: roomCode,
                            timer: timer,
                            team_color: data.recent.team_color,
                            pole_number: data.recent.pole_number,
                        },
                    });
                    xhr.done(function (response) {
                        console.log(response.message);
                    });
                }
                //paint pole color
                //find the pole
                let $poleEle = $('[data-pole-number="' + data.recent.pole_number + '"]');
                //remove white background
                if (data.recent.team_color === 'red') {
                    $poleEle.removeClass('bg-white bg-red bg-blue').addClass('bg-red');
                    $poleEle.data('lock-type',1)
                } else {
                    $poleEle.removeClass('bg-white').addClass('bg-blue');
                    $poleEle.data('lock-type',2)
                }
            }

            // console.log(data.recent.pole_number)
            // console.log(data.recent.team_color)
        }
        let $cheyYoWrapper = $("#chey_yo_modal");
        let $cheyYo = $(".chey_yo_content");
        let $dataTimerPause = $("#timer_pause");

        // Chey yo part

        console.log(nRedPoles);
        if (isCheyyoEnable) {
            if (nRedPoles === 8) {
                console.log("popup");
                $cheyYo.addClass("bg-red");
                $cheyYoWrapper.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
                alertStopToScoreOperation("Red Team")
            }
            if (nBluePoles === 8) {
                $cheyYo.addClass("bg-bleu");
                $cheyYoWrapper.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
                alertStopToScoreOperation("Blue Team")
            }
        }
    });
    let resetScoreAndPoleChannel = pusher.subscribe("resetScore");
    resetScoreAndPoleChannel.bind("reset",function (data) {

        if (data.room_code === roomCode) {
            //reset the lock
            $('.ring_pole').data('lock-type',0)
            //paint the pole
            paintThePole();
        }
    })

    // cheyyo trigger channel
    let cheyyotriggerChannel = pusher.subscribe("cheyyoChannel");
    cheyyotriggerChannel.bind("alert",function (data) {
        let $cheyYoWrapper = $("#chey_yo_modal");
        let $cheyYo = $(".chey_yo_content");
        let $dataTimerPause = $("#timer_pause");

        if (data.room_code === roomCode) {
            if (data.team_color === 'red') {
                console.log("popup");
                $cheyYo.addClass("bg-red");
                $cheyYoWrapper.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
                alertStopToScoreOperation("Red Team")
            }
            if (data.team_color === 'blue') {
                $cheyYo.addClass("bg-bleu");
                $cheyYoWrapper.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
                alertStopToScoreOperation("Blue Team")
            }
        }
    })

    // TODO: ENABLE
    // disableCountDownPrepareDisplay();
    performTimerStart();
    paintThePole();
    // Function to display the countdown in MM:SS format
    function displayCountdown(countdown)
    {
        if (countdown === 10) {
            audioAlmostFinish.get(0).play();
        }
        let minutes = Math.floor(countdown / 60);
        let seconds = countdown % 60;
        let formattedSeconds = seconds < 10 ? "0" + seconds : seconds;
        //sending request to broadcast

        $("#timer_display").text("0" + minutes + ":" + formattedSeconds);
    }
    function displayCountdownPrepare(countdown)
    {
        $("#countdown_prepare").text(countdown);
    }

    function disableCountDownPrepareDisplay()
    {
        $("#score_board").removeClass("d-none");
        $("#countdown_prepare_div").addClass("d-none");
    }
    function performTimerStart()
    {
        let value = timerStart.val();

        let checkChangeAndStartTimer = function () {
            let latestValue = timerStart.val();

            if (latestValue !== value) {
                console.log("perform start timer");
                let countdownInterval;
                let minutes = $("#timer").val(); // Set the countdown time in minutes
                let countdown = minutes * 60 + 10; // Convert minutes to seconds // plus 1 because delay 1
                let initialCountDown = countdown;
                // let noPlay = 0;
                let countdownIntervalPrepare;
                let countdownPrepare = parseInt($("#countdown_prepare").text());
                audioPrepare.get(0).play();
                //Display prepare countdown
                displayCountdownPrepare(countdownPrepare);

                //start the countdown prepare
                countdownIntervalPrepare = setInterval(function () {
                    //play the sound alert
                    // console.log($('#prepare_sound').attr('src'));
                    countdownPrepare--;
                    displayCountdownPrepare(countdownPrepare);
                    // Countdown is finished
                    if (countdownPrepare === 0) {
                        clearInterval(countdownIntervalPrepare);
                        $("#countdown_prepare_div").addClass("d-none");
                        $("#score_board").removeClass("d-none");
                    }
                }, 1000);

                // Display the initial countdown time
                displayCountdown(countdown);

                // Start the countdown
                countdownInterval = setInterval(function () {
                    countdown--;
                    if (
                        countdown > 0 &&
                        countdown % 60 === 0 &&
                        countdown < initialCountDown - 11
                    ) {
                        // noPlay = 1
                        audioStart.get(0).play();
                    }
                    if (countdown < initialCountDown - 10) {
                        rewriteResultToJson();
                    }

                    displayCountdown(countdown);
                    let timerPause = $("#timer_pause").data("timer-pause");
                    if (timerPause) {
                        rewriteResultToJson();
                        clearInterval(countdownInterval);
                    }
                    /*if(countdown === 10){
                        audioAlmostFinish.get(0).play()
                    }*/
                    // Countdown is finished
                    if (countdown === 0) {
                        audioFinish.get(0).play();
                        rewriteResultToJson();
                        clearInterval(countdownInterval);
                    }
                }, 1000);
            } else {
                console.log("still check timer");
                setTimeout(checkChangeAndStartTimer, 400); // check every 2s, change here if we wish to check faster
            }
        };
        checkChangeAndStartTimer();
    }
    function paintThePole()
    {
        let $this = $('.ring_pole')
        $this.each(function () {
            let colorType = $(this).data('lock-type');
            //lock type 1 = red, 2 = blue
            console.log(colorType)
            if (colorType === 1) {
                // console.log('hello')
                $(this).removeClass('bg-white').addClass('bg-red')
            } else if (colorType === 2) {
                $(this).removeClass('bg-white').addClass('bg-blue')
            } else {
                $(this).removeClass('bg-red bg-blue').addClass('bg-white')
            }
        })
    }
    function rewriteResultToJson()
    {
        let timer = $('#timer_display').text();
        let xhr = $.ajax({
            method: "post",
            url: "/matches/rewriteMatchResultToJsonFile",
            dataType: "json",
            data: {
                room_code: roomCode,
                timer: timer,
            },
        });
        /*xhr.done(function (response) {
            console.log(response.message);
        });*/
    }
    function alertStopToScoreOperation($teamColor)
    {
        let xhr = $.ajax({
            method: "post",
            url: "/matches/alertStopToScoreOperation",
            dataType: "json",
            data: {
                room_code: roomCode,
                team_color: $teamColor
            },
        });
    }
});
