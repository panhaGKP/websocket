$(document).ready(function () {
    console.log('hi')
    let isCheyyoEnable = $('#is_cheyyo_enable').data('cheyyo-enable');
    console.log(isCheyyoEnable)
    if (isCheyyoEnable) {
        console.log('cheyyo enable')
    } else {
        console.log('cheyyo disable')
    }
    // Set up Pusher
    let pusher = new Pusher("1eaf2f48999ec6fdee2b", { //YOUR_APP_KEY
        cluster: "ap1", // YOUR_APP_CLUSTER
        encrypted: true
    });
    let roomCode = $('#roomCode').data('room-code');
    let channelMatchScoreAndPole = pusher.subscribe("matchScoreAndPole");
    channelMatchScoreAndPole.bind("update",function (data) {
        // console.log(timer)
        // console.log(redFirstOwn +' : '+blueFirstOwn)
        // console.log(data)
        let nRedPoles = data.number_of_red_pole;
        let nBluePoles = data.number_of_blue_pole;
        if (data.room_code === roomCode) {
            $('#red_score').text(data.red_score);
            $('#blue_score').text(data.blue_score);
            $('#red_pole').text(data.number_of_red_pole);
            $('#blue_pole').text(data.number_of_blue_pole);

            //paint pole color
            //find the pole
            let $poleEle = $('[data-pole-number="' + data.recent.pole_number + '"]');
            //remove white background
            if (data.recent.team_color === 'red') {
                $poleEle.removeClass('bg-white bg-red bg-blue').addClass('bg-red');
                $poleEle.data('lock-type',1)
            } else {
                $poleEle.removeClass('bg-white').addClass('bg-blue');
                $poleEle.data('lock-type',2)
            }
        }
        let $dataTimerPause = $('#timer_pause');
        let $cheyYo = $(".chey_yo_content");
        let $cheyYoContainer = $('.chei_yo_container');

        if (isCheyyoEnable) {
            if (nRedPoles === 8) {
                console.log("popup");
                $cheyYo.addClass("bg-red");
                $cheyYoContainer.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
            }
            if (nBluePoles === 8) {
                $cheyYo.addClass("bg-blue");
                $cheyYoContainer.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
            }
        }
    })
    let resetScoreAndPoleChannel = pusher.subscribe("resetScore");
    resetScoreAndPoleChannel.bind("reset",function (data) {
        if (data.room_code === roomCode) {
            //reset the lock
            $('.ring_pole').data('lock-type',0)
            //paint the pole
            paintThePole();
        }
    })

    // cheyyo trigger channel
    let cheyyotriggerChannel = pusher.subscribe("cheyyoChannel");
    cheyyotriggerChannel.bind("alert",function (data) {
        let $dataTimerPause = $('#timer_pause');
        let $cheyYo = $(".chey_yo_content");
        let $cheyYoContainer = $('.chei_yo_container');
        if (data.room_code === roomCode) {
            if (data.team_color === 'red') {
                console.log("popup");
                $cheyYo.addClass("bg-red");
                $cheyYoContainer.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
            }
            if (data.team_color === 'blue') {
                $cheyYo.addClass("bg-blue");
                $cheyYoContainer.removeClass("d-none");
                $dataTimerPause.data("timer-pause", 1);
            }
        }
    })

    // predefined timer
    let $timerDisplay = $('#timer_display');
    let $timeValue = $('#timer');
    console.log($timeValue.val())
    $timerDisplay.text('0' + $timeValue.val() + ':00')

    let timerStart = $('#timerstart');
    let channelTimer = pusher.subscribe("timer");
    channelTimer.bind("update", function (data) {
        /*console.log('hello')
        console.log(data)
        console.log(roomCode)*/
        if (roomCode === data.roomCode) {
            timerStart.val(data.timerStarted);
        }
    });
    performTimerStart();
    paintThePole();
    function displayCountdown(countdown)
    {
        let minutes = Math.floor(countdown / 60);
        let seconds = countdown % 60;
        let formattedSeconds = seconds < 10 ? '0' + seconds : seconds;
        $('#timer_display').text('0' + minutes + ':' + formattedSeconds);
    }
    function performTimerStart()
    {
        let value = timerStart.val();

        let checkChangeAndStartTimer = function () {
            let latestValue = timerStart.val();

            if (latestValue !== value) {
                console.log('perform start timer')
                let countdownInterval;
                let minutes = $('#timer').val(); // Set the countdown time in minutes
                let countdown = minutes * 60 + 11; // Convert minutes to seconds // plus 1 because delay 1
                let initialCountDown = countdown;
                // let noPlay = 0;
                // Display the initial countdown time
                //displayCountdown(countdown);

                // Start the countdown
                countdownInterval = setInterval(function () {

                    countdown--;
                    if (countdown > (initialCountDown - 11)) {
                        // noPlay = 1
                       // audioStart.get(0).play()
                        console.log('just a prepare time')

                        return;
                    }

                    displayCountdown(countdown);
                    let timerPause = $('#timer_pause').data('timer-pause')
                    if (timerPause) {
                        clearInterval(countdownInterval);
                    }
                    // Countdown is finished
                    if (countdown === 0) {
                        //audioFinish.get(0).play()
                        clearInterval(countdownInterval);
                    }
                }, 1000);
            } else {
                console.log('still check timer');
                setTimeout(checkChangeAndStartTimer,400); // check every 2s, change here if we wish to check faster
            }
        }
        checkChangeAndStartTimer();
    }
    function paintThePole()
    {
        let $this = $('.ring_pole')
        $this.each(function () {
            let colorType = $(this).data('lock-type');
            //lock type 1 = red, 2 = blue
            console.log(colorType)
            if (colorType === 1) {
                // console.log('hello')
                $(this).removeClass('bg-white').addClass('bg-red')
            } else if (colorType === 2) {
                $(this).removeClass('bg-white').addClass('bg-blue')
            } else {
                $(this).removeClass('bg-red bg-blue').addClass('bg-white')
            }
        })
    }
})
