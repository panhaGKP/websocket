$(document).ready(function () {
    console.log("Red score operation");
    // $('#alertStopOperation').modal('show');
    // init
    let roomCode = $("#room_code").data("room-code");
    let pusher = new Pusher("1eaf2f48999ec6fdee2b", {
        cluster: "ap1",
        // encrypted: true,
    });

    // setup connection response
    let connectionRespond = pusher.subscribe("respondConnection");
    connectionRespond.bind("test", function (data) {
        console.log(data);
        if (parseInt(data.connection_red)) {
            $("#connection").data("connection", data.connection);
            flashSuccess();
        }
    });

    // init locks
    applyBgToLockButton();

    // TODO: Start timer should be control by another party
    $("#start-timer").on("click", function () {
        console.log("start time started");
        let roomCode = $("#room_code").data("room-code");
        console.log(roomCode);
        $.ajax({
            url: "/matches/startTimer",
            method: "post",
            dataType: "json",
            data: {
                roomCode: roomCode,
            },
        });
    });

    // set onClick event
    $(".red_circle").on("click", function () {
        let $lastLockEle = $("#last-lock-issued");
        let issue_time = Date.now();
        $lastLockEle.data("last-lock-issued", issue_time);

        let parentDiv = $(this).parent();
        console.log("red_circle, click");

        // set bg-color to main
        $(this).addClass("red");

        // remove alt bg-color
        let otherCircle = parentDiv.find(".blue_small_circle");
        otherCircle.removeClass("blue");

        // set main lock
        let thisLock = $(this).data("lock");
        let otherLock = otherCircle.data("lock");
        // $(this).data('lock', 1);
        // remove alt lock
        // otherCircle.data('lock', 0);

        let roomCode = $("#room_code").data("room-code");
        let poleNumber = parentDiv.data("pole-number");
        let score = $(this).data("score");
        let teamColor = "red";

        if (thisLock) {
            console.log(`[INFO]: ${poleNumber} already locked`);
            return;
        }

        if (poleNumber === 1 || poleNumber === 2 || poleNumber === 3) {
            let xhr1 = $.ajax({
                method: "post",
                url: "/matches/scoreForPole123",
                dataType: "json",
                data: {
                    pole_number: poleNumber,
                    team_color: teamColor,
                    score: score,
                    room_code: roomCode,
                    issue_time: issue_time,
                },
            });

            xhr1.done((data) => {
                console.log(data);
                if (!data.isLock) {
                    console.log("unlock it back");
                    $(this).removeClass("red");
                    otherCircle.addClass("blue");

                    $(this).data("lock", 0);
                    otherCircle.data("lock", 1);
                    return;
                }
            });
        } else if (
            poleNumber === 4 ||
            poleNumber === 5 ||
            poleNumber === 6 ||
            poleNumber === 7 ||
            poleNumber === 8
        ) {
            let xhr1 = $.ajax({
                method: "post",
                url: "/matches/scoreForPole45678",
                dataType: "json",
                data: {
                    pole_number: poleNumber,
                    team_color: teamColor,
                    score: score,
                    room_code: roomCode,
                    issue_time: issue_time,
                },
            });

            xhr1.done((data) => {
                console.log(data);
                if (!data.isLock) {
                    console.log("unlock it back");
                    $(this).removeClass("red");
                    otherCircle.addClass("blue");

                    $(this).data("lock", 0);
                    otherCircle.data("lock", 1);
                    return;
                }
            });
        } else {
            let xhr1 = $.ajax({
                method: "post",
                url: "/matches/scoreForPole91011",
                dataType: "json",
                data: {
                    pole_number: poleNumber,
                    team_color: teamColor,
                    score: score,
                    room_code: roomCode,
                    issue_time: issue_time,
                },
            });

            xhr1.done((data) => {
                console.log(data);
                if (!data.isLock) {
                    console.log("unlock it back");
                    $(this).removeClass("red");
                    otherCircle.addClass("blue");

                    $(this).data("lock", 0);
                    otherCircle.data("lock", 1);
                    return;
                }
            });
        }

        console.log(`[INFO]: ${poleNumber} locked`);
        $(this).data("lock", 1);
        otherCircle.data("lock", 0);
    });

    $("#test-connection").on("click", function () {
        let $connectionElement = $("#connection");
        let connection = $connectionElement.data("connection");
        let timeInterval = 5;
        let xhr1 = $.ajax({
            method: "post",
            url: "/matches/broadcastRequestConnection",
            dataType: "json",
            data: {
                room_code: roomCode,
                connection_red: 1,
            },
        });

        let connectionInterval = setInterval(function () {
            timeInterval--;
            if (timeInterval === 0) {
                clearInterval(connectionInterval);
                let newConnectionTest = $("#connection").data("connection");
                if (!parseInt(newConnectionTest)) {
                    flashFail();
                }
                $connectionElement.data("connection", 0);
            }
        }, 1000);
    });

    $("#reset-score").on("click", function () {
        $(".confirm-letter").addClass("show");
        $("#loading-confirm").removeClass("d-none");
        $(this).addClass("disabled");
        console.log(roomCode);
        let xhr1 = $.ajax({
            method: "get",
            url: "/matches/resetScoreMatch/" + roomCode,
            dataType: "json",
            data: {},
        });
        xhr1.done(function (response) {
            console.log(parseInt(response.roomCode) !== roomCode);
            if (parseInt(response.roomCode) !== roomCode) {
                return;
            }
            if (parseInt(response.code) === 200) {
                let $circle = $(".circle");
                //reset lock
                $circle.data("lock", 0);
                //reset bg
                $circle.removeClass("red blue");
                $("#resetScoreModal").modal("hide");

                $(".confirm-letter").removeClass("d-none");
                $("#loading-confirm").addClass("d-none");
                $("#reset-score").removeClass("disabled");
            }
        });
    });

    let blueOperationLock = pusher.subscribe("blueToRedScoreOperation");
    blueOperationLock.bind("update", function (data) {
        console.log(data);
        if (roomCode === data.room_code) {
            // let $lastLockEle = $("#last-lock-issued");
            // if (data.issue_time < $lastLockEle.data("last-lock-issued")) {
            //     console.log("Old lock, ignored");
            //     return;
            // }

            //find that pole using pole number
            let $poleEle = $('[data-pole-number="' + data.pole_number + '"]');
            // find big red circle and unlock it
            $poleEle.find(".red_circle").data("lock", 0);
            //find small blue circle and lock it
            $poleEle.find(".blue_small_circle").data("lock", 1);
            // reset the lock background
            applyBgToLockButton();
        }
    });

    let resetScoreChannel = pusher.subscribe("resetScore");
    resetScoreChannel.bind("reset", function (data) {
        console.log("Reset Score Event Received");
        //console.log(data);

        if (roomCode === data.room_code) {
            console.log("Reset Score Event Processed");
            let $circle = $(".red_circle, .blue_small_circle");
            // reset locks
            $circle.data("lock", 0);
            // reset bg
            $circle.removeClass("red blue");
            // reset score
        }
    });

    let stopOperationAlertChannel = pusher.subscribe("stopOperation");
    stopOperationAlertChannel.bind("alert",function(data) {
        if (roomCode === data.room_code) {
            $('#who_chey_yo').text(data.team_color);
            $('#alertStopOperation').modal('show');
        }
    })
    function flashSuccess() {
        $("#success-flash").addClass("show");
    }

    function flashFail() {
        $("#fail-flash").addClass("show");
    }

    function applyBgToLockButton() {
        let $this = $(".red_circle, .blue_small_circle");
        $this.each(function () {
            let bgColorLock = $(this).data("color");
            let isLock = $(this).data("lock");
            if (isLock) {
                $(this).addClass(bgColorLock);
            } else {
                $(this).removeClass(bgColorLock);
            }
        });
    }
});
